@extends('layouts.master')
@section('content')



<div class="my-3 my-md-5">
    <div class="container">
        <div class="page-header">
            <h1 class="page-title">
			  {{ $title }}
             </h1>
        </div>
	<form class="card">
		<div class="card-header">
			<h3 class="card-title">Form Control</h3>
		</div>   
        <div class="card-body">
				                 
				  <div class="row">
                     <div class="col-sm-6 col-md-12">
                        <div class="col-md-6">
							<span style="vertical-align: top;">
                            <a href="{{ URL::to('time_admin?nopeg='.$param['nopeg_maintain'].'&month='.$param['month_bef']."&year=".$param['year_bef'])}}">
									<i class="fa fa-arrow-circle-left" data-name="angle-double-left" data-size="24"  data-c="#418BCA" data-hc="blue"></i>
								</a>
									{{$param['smonth']}} {{$param['year']}}
                                    <a href="{{ URL::to('time_admin?nopeg='.$param['nopeg_maintain'].'&month='.$param['month_next']."&year=".$param['year_next'])}}">
									<i class="fa fa-arrow-circle-right" data-name="angle-double-right" data-size="24"  data-c="#418BCA" data-hc="blue"></i>
								</a>
							</span>
						</div>
					</div>

                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                          <label class="form-label">
                          Emp.Number : {{ $param['nopeg_maintain'] }}
                          </label>
                          <select style="width: 100%" name="fNopeg" class="form-control select2me" id="fNopeg">
                                <?php
                                foreach ($param['empMaintain'] as $oEmp) {
                                    $selected="";
                                    if($param['nopeg_maintain']==$oEmp->nopeg_maintain){
                                        $selected="selected";
                                    }
                                    ?>
                                        <option value="{{ $oEmp->nopeg_maintain }}" {{ $selected }}>{{ $oEmp->getNama() }}</option>                        
                                    <?php                                 
                                }
                                ?>
                            </select>
                            </div>
                      </div>
                      <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                          <label class="form-label">
                          Position : {{$pos['jabatan'] }}
                          </label>
						  <label class="form-label">
                          Unit : {{ $pos['unit'] }}
                          </label>
                        </div>
                      </div>
               
					 
                    <div class='col-md-1'>
                        <a href="{{ URL::to('dl_time_data?month='.$param['month']."&year=".$param['year'])}}">
                            <i class="fe fe-download-cloud" data-name="download" data-size="24"  data-c="#418BCA" data-hc="blue"></i>
                            Download
                        </a>
                        </a>
                    </div>
				 </div>
	        </div>
	</form>
    </div>
 </div>


			<div class="row row-cards row-deck">
              <div class="col-12">
                <div class="card">
                  <div class="table-responsive">
					<table class="table table-hover table-outline table-vcenter text-nowrap card-table">
						<thead>
                            <tr>
                                <!--<th class="numeric">num</th>-->
                                <th>Day(Date)</th>
                                <th>DWS(Work Hours)</th>
                                <th class="numeric">Clock In</th>
                                <th class="numeric">Clock Out</th>
                                <th class="numeric">Late|Early|Total (HH:MM:SS)</th>
                                <th class="numeric">Reason</th>
                            </tr>
						</thead>
                        <tbody>
                            @if (count($timedata) > 0)
                                @foreach ($timedata as $data)
                                <tr>
                                    <td>{{$data->tanggal}}</td>
                                    <td>{{$data->dws}} ({{$aKey[$data->dws]->jam_masuk}} - {{$aKey[$data->dws]->jam_keluar}}) </td>
                                    <td class="numeric">{{$data->clock_in}}</td>
                                    <td class="numeric">{{$data->clock_out}}</td>
                                    <td class="numeric">{{ $data->getHMSComeLate() }} | {{ $data->getHMSBackEarly() }} | {{ $data->getHMSTotal() }}</td>
                                    <td class="numeric">{{$aReason[$data->data_id]}}</td>
                                </tr>
                                @endforeach
                            @else
                            <tr>
                                <td colspan='6'>no data</td>
                            </tr>
                            
                            @endif


                        </tbody>
					</table>
                  </div>
                </div>
              </div>
            </div>


<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("select.fNopeg2").change(function(){
        var sNopeg = $(this).val();
        window.location.assign("{{ URL::to('time_admin')}}?month={{ $param['month']}}&year={{$param['year']}}&nopeg="+sNopeg)
    });
});
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script type="text/javascript">
 $( document ).ready(function() {
   $('select[name=fNopeg]').on("change", function(e){
    var sNopeg = $(this).val();

          window.location.assign("{{ URL::to('time_admin')}}?month={{ $param['month']}}&year={{$param['year']}}&nopeg="+sNopeg)
   });
});
</script>

@stop
