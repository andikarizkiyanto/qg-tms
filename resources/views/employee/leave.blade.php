@extends('layouts.master')
@section('content')



<div class="my-3 my-md-5">
    <div class="container">
        <div class="page-header">
            <h1 class="page-title">
			  {{ $title }}
             </h1>
        </div>
	<form class="card">
		<div class="card-header">
			<h3 class="card-title">Form Control</h3>
		</div>   
        <div class="card-body">
				                 
				  <div class="row">
                      <div class="col-sm-6 col-md-2">
                        <div class="form-group">
                          <label class="form-label">2018</label>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                          <label class="form-label">Emp. Number: {{ $sNopeg }} </label>
						   <label class="form-label">Name : {{ $nama }}</label>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                          <label class="form-label">Position : </label>
						  <label class="form-label">Unit : </label>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-2">
                        <div class="form-group">
                          <label class="form-label">Leave Entl/Remn : </label>
                          <label class="form-label">CS Entl/Remn : </label>
                        </div>
                      </div>
                     
					 
		<div class="card-footer text-right">
            <div class="d-flex">
                <a class="btn btn-primary btn-large" data-toggle="modal" data-href="#createLeave" href="#createLeave">Create Leave</a>
             </div>
        </div>
					                 
	</form>
         </div>
         </div>


<div class="row row-cards row-deck">
              <div class="col-12">
                <div class="card">
                  <div class="table-responsive">
<table class="table table-hover table-outline table-vcenter text-nowrap card-table">
			<thead>
				<tr>
					<th>#</th>
					<th>Leave Type</th>
					<th>Start-End</th>
					<th>Reason</th>
					<th>Sup. Reason</th>
					
				</tr>
			</thead>
			<tbody>
				@php(
					$no = 1 {{-- buat nomor urut --}}
					)
				{{-- loop all kendaraan --}}
				@foreach ($leaves as $leave)
					<tr>
						<td>{{ $no++ }}</td>
						<td>{{ $leave->leave_id }}</td>
						<td>{{ $leave->start_date }}</td>
						<td>{{ $leave->idh_reason }}</td>
						<td>{{ $leave->sup_reason }}</td>
					

					
					</tr>
				@endforeach
				{{-- // end loop --}}
			</tbody>
</table>
                  </div>
                </div>
              </div>


<div class="modal fade in" id="createLeave"  role="dialog" aria-hidden="false" style="display:none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form class="form-horizontal" action="" method="post" id="formLeave" enctype="multipart/form-data">
                <input type="hidden" name='type' value='leaveprop'/>
                {!! csrf_field() !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Create Leave</h4>
                </div>
                <div class="modal-body">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Superior</label>
                            <div class="col-md-9">
                                <input id="fsuperior" name="fsuperior" type="text" placeholder="Superior Nopeg & Name" value="" class="form-control" disabled></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Employee</label>
                            <div class="col-md-9">
                                <input id="fNopeg" name="fNopeg" type="text" placeholder="Your Nopeg & Name" value="" class="form-control" disabled></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Leave Type</label>
                            <div class="col-md-9">
                                <select style="width: 100%" name="fAbs" class="form-control select2me" id="fAbs">
                             
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="fileupload">
                            <label class="col-md-3 control-label" for="document">Document</label>
                            <div class="col-md-9" id="dDok">
                                <input class="form-control" id="inputfile" name='inputfile' type="file"  data-allowed-file-extensions='["jpg","jpeg","bmp","png"]' style="height: 27px;">
                            </div>
                        </div>
                        <!-- Email input-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="email">Date</label>
                            <div class="col-md-9">
                                <input id="fTanggal" name="fTanggal" type="text" placeholder="DD.MM.YYYY"  class="form-control">
                            </div>
                        </div>
                        <!-- Message body -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="message">Your message</label>
                            <div class="col-md-9">
                                <textarea class="form-control" id="message" name="message" placeholder="Please enter your message here..." rows="5"></textarea>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn">Close</button>
                    <button type="submit" class="btn btn-primary">Propose</button>
                </div>
            </form>
        </div>
    </div>
</div>



@stop
