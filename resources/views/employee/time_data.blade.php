@extends('layouts.master')
@section('content')



<div class="my-3 my-md-5">
    <div class="container">
        <div class="page-header">
            <h1 class="page-title">
			  {{ $title }}
             </h1>
        </div>
	<form class="card">
		<div class="card-header">
			<h3 class="card-title">Form Control</h3>
		</div>   
        <div class="card-body">
				                 
				  <div class="row">
                     <div class="col-sm-6 col-md-12">
                        <div class="col-md-6">
							<span style="vertical-align: top;">
								<a href="{{ URL::to('time_data?month='.$param['month_bef']."&year=".$param['year_bef'])}}">
									<i class="fa fa-arrow-circle-left" data-name="angle-double-left" data-size="24"  data-c="#418BCA" data-hc="blue"></i>
								</a>
									{{$param['smonth']}} {{$param['year']}}
								<a href="{{ URL::to('time_data?month='.$param['month_next']."&year=".$param['year_next'])}}">
									<i class="fa fa-arrow-circle-right" data-name="angle-double-right" data-size="24"  data-c="#418BCA" data-hc="blue"></i>
								</a>
							</span>
						</div>
					</div>

                      <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                          <label class="form-label">Emp. Number: {{ $pos['nopeg'] }} </label>
						   <label class="form-label">Name : {{ $pos['nama'] }}</label>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                          <label class="form-label">Position : {{ $pos['jabatan'] }}</label>
						  <label class="form-label">Unit : {{ $pos['unit'] }}</label>
                        </div>
                      </div>
               
					 
                    <div class='col-md-1'>
                        <a href="{{ URL::to('dl_time_data?month='.$param['month']."&year=".$param['year'])}}">
                            <i class="fe fe-download-cloud" data-name="download" data-size="24"  data-c="#418BCA" data-hc="blue"></i>
                            Download
                        </a>
                        </a>
                    </div>
				 </div>
	        </div>
	</form>
    </div>
 </div>


			<div class="row row-cards row-deck">
              <div class="col-12">
                <div class="card">
                  <div class="table-responsive">
					<table class="table table-hover table-outline table-vcenter text-nowrap card-table">
						<thead>
                            <tr>
                                <!--<th class="numeric">num</th>-->
                                <th>Day(Date)</th>
                                <th>DWS(Work Hours)</th>
                                <th class="numeric">Clock In</th>
                                <th class="numeric">Clock Out</th>
                                <th class="numeric">Late|Early|Total (HH:MM:SS)</th>
                                <th class="numeric">Reason</th>
                            </tr>
						</thead>
                        <tbody>
                            @if (count($timedata) > 0)
                                @foreach ($timedata as $data)
                                <tr>
                                    <td>{{$data->tanggal}}</td>
                                    <td>{{$data->dws}} ({{$aKey[$data->dws]->jam_masuk}} - {{$aKey[$data->dws]->jam_keluar}}) </td>
                                    <td class="numeric">{{$data->clock_in}}</td>
                                    <td class="numeric">{{$data->clock_out}}</td>
                                    <td class="numeric">{{ $data->getHMSComeLate() }} | {{ $data->getHMSBackEarly() }} | {{ $data->getHMSTotal() }}</td>
                                    <td class="numeric">{{$aReason[$data->data_id]}}</td>
                                </tr>
                                @endforeach
                            @else
                            <tr>
                                <td colspan='6'>no data</td>
                            </tr>
                            
                            @endif


                        </tbody>
					</table>
                  </div>
                </div>
              </div>
            </div>


<div class="modal fade in" id="createLeave"  role="dialog" aria-hidden="false" style="display:none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form class="form-horizontal" action="" method="post" id="formLeave" enctype="multipart/form-data">
                <input type="hidden" name='type' value='leaveprop'/>
                {!! csrf_field() !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Create Leave</h4>
                </div>
                <div class="modal-body">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Superior</label>
                            <div class="col-md-9">
                                <input id="fsuperior" name="fsuperior" type="text" placeholder="Superior Nopeg & Name" value="" class="form-control" disabled></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Employee</label>
                            <div class="col-md-9">
                                <input id="fNopeg" name="fNopeg" type="text" placeholder="Your Nopeg & Name" value="" class="form-control" disabled></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="name">Leave Type</label>
                            <div class="col-md-9">
                                <select style="width: 100%" name="fAbs" class="form-control select2me" id="fAbs">
                             
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="fileupload">
                            <label class="col-md-3 control-label" for="document">Document</label>
                            <div class="col-md-9" id="dDok">
                                <input class="form-control" id="inputfile" name='inputfile' type="file"  data-allowed-file-extensions='["jpg","jpeg","bmp","png"]' style="height: 27px;">
                            </div>
                        </div>
                        <!-- Email input-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="email">Date</label>
                            <div class="col-md-9">
                                <input id="fTanggal" name="fTanggal" type="text" placeholder="DD.MM.YYYY"  class="form-control">
                            </div>
                        </div>
                        <!-- Message body -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="message">Your message</label>
                            <div class="col-md-9">
                                <textarea class="form-control" id="message" name="message" placeholder="Please enter your message here..." rows="5"></textarea>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn">Close</button>
                    <button type="submit" class="btn btn-primary">Propose</button>
                </div>
            </form>
        </div>
    </div>
</div>



@stop
