<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="./favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
    <!-- Generated: 2018-04-06 16:27:42 +0200 -->
    <title>Time Management System - {{ $title }}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    <script src="{{ asset('/assets/js/require.min.js') }}"></script>
    <script>
      requirejs.config({
          baseUrl: '.'
      });
    </script>
    <!-- Dashboard Core -->
	
    <link href="{{ asset('/assets/css/dashboard.css') }}" rel="stylesheet" />
    <script src="{{ asset('/assets/js/dashboard.js') }}"></script>
    <!-- c3.js Charts Plugin -->
    <link href="{{ asset('/assets/plugins/charts-c3/plugin.css') }}" rel="stylesheet" />
    <script src="{{ asset('/assets/plugins/charts-c3/plugin.js') }}"></script>
    <!-- Google Maps Plugin -->
    <link href="{{ asset('/assets/plugins/maps-google/plugin.css') }}" rel="stylesheet" />
    <script src="{{ asset('/assets/plugins/maps-google/plugin.js') }}"></script>
    <!-- Input Mask Plugin -->
    <script src="{{ asset('/assets/plugins/input-mask/plugin.js') }}"></script>
</head>
  
  
  <body class="">
    <div class="page">
      <div class="page-main">
        <div class="header py-4">
          <div class="container">
            <div class="d-flex">
              <a class="header-brand" href="./index.html">
				GA - Time Management System
              </a>
              <div class="d-flex order-lg-2 ml-auto">
              
              
                <div class="dropdown">
                  <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                    <span class="avatar" style="background-image: url(./demo/faces/female/25.jpg)"></span>
                    <span class="ml-2 d-none d-lg-block">
                      <span class="text-default"> {{ $pos['nama'] }}</span>
                      <small class="text-muted d-block mt-1">{{ $pos['jabatan'] }}</small>
                    </span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                    <a class="dropdown-item" href="#">
                      <i class="dropdown-icon fe fe-user"></i> Profile
                    </a>
                    <a class="dropdown-item" href="#">
                      <i class="dropdown-icon fe fe-settings"></i> Settings
                    </a>
                    <a class="dropdown-item" href="#">
                      <span class="float-right"><span class="badge badge-primary">6</span></span>
                      <i class="dropdown-icon fe fe-mail"></i> Inbox
                    </a>
                    <a class="dropdown-item" href="#">
                      <i class="dropdown-icon fe fe-send"></i> Message
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                      <i class="dropdown-icon fe fe-help-circle"></i> Need help?
                    </a>
                    <a class="dropdown-item" href="{{ route('logout') }}">
        {{ csrf_field() }}
                      <i class="dropdown-icon fe fe-log-out"></i> Sign out
                    </a>
                  </div>
                </div>
              </div>
              <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
                <span class="header-toggler-icon"></span>
              </a>
            </div>
          </div>
        </div>
        <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-lg-3 ml-auto">
                <form class="input-icon my-3 my-lg-0">
                  <input type="search" class="form-control header-search" placeholder="Search&hellip;" tabindex="1">
                  <div class="input-icon-addon">
                    <i class="fe fe-search"></i>
                  </div>
                </form>
              </div>
              <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                  <li class="nav-item">

                 <?php
                  $active = "";
                  ?>
                  @if($nav=="Home")
                  <?php $active="active"   ?>                    
                  @endif    

                    <a href="./" class="nav-link {{$active}}"><i class="fe fe-home"></i> Home</a>
                  </li>
                  <li class="nav-item">

                  <?php
                  $active = "";
                  ?>
                  @if($nav=="Employee")
                  <?php $active="active"   ?>                    
                  @endif    

                    <a href="javascript:void(0)" class="nav-link  {{$active}}" data-toggle="dropdown"><i class="fe fe-user"></i> Employee</a>
                    <div class="dropdown-menu dropdown-menu-arrow">
                      <a href="./time_data" class="dropdown-item ">Time Data</a>
                    </div>
                  </li>
                  @if( Auth::user()->isTimeAdmin)

                  <li class="nav-item dropdown">

                  <?php
                  $active = "";
                  ?>
                  @if($nav=="Time Admin")
                  <?php $active="active"   ?>                    
                  @endif   

                    <a href="javascript:void(0)" class="nav-link  {{$active}}" data-toggle="dropdown"><i class="fe fe-calendar"></i> Time Admin</a>
                    <div class="dropdown-menu dropdown-menu-arrow">
                      <a href="./time_admin" class="dropdown-item ">Employee(s) Time Data</a>

                    </div>
                  </li>

                      @endif

                  <li class="nav-item dropdown">

                  <?php
                  $active = "";
                  ?>
                  @if($nav=="Superior")
                  <?php $active="active"   ?>                    
                  @endif   

                    <a href="javascript:void(0)" class="nav-link  {{$active}}" data-toggle="dropdown"><i class="fe fe-calendar"></i> Superior Approval</a>
                    <div class="dropdown-menu dropdown-menu-arrow">
                      <a href="./suptime_data" class="dropdown-item ">Subordinat Time Data</a>

                    </div>
                  </li>
                    

                </ul>
              </div>
            </div>
          </div>
        </div>
	</body>