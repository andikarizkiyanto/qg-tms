<?php

namespace App;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Librarys\LibStaticConnect;
use App\Librarys\LibStaticContent;
use Carbon\Carbon;
use App\TMSCutiTangguh;
use App\TMSEmpData;


/**
 * Description of CoreUserRole
 *
 * @author user
 */
class TMSValidate extends Model {

    //put your code here
    protected $table = 'tms_validate';
    protected $primaryKey = 'val_id';

    public function getStatusText() {
        return LibStaticContent::getStatusTextType1($this->status);
    }

    public function getData() {
        return TMSEmpData::where('data_id', $this->data_id)->first();
    }

    public function data() {
        return $this->belongsTo('App\TMSEmpData');
    }

    public function getTimeValidationForView() {
        $sRet = "";
        if (!empty($this->clock_in_val)) {
            $sRet = $this->clock_in_val;
        }
        if (!empty($this->clock_out_val)) {
            if (!empty($sRet)) {
                $sRet.=" | ";
            }
            $sRet.=$this->clock_out_val;
        }
        return $sRet;
    }

    public function isPropose() {
        if ($this->status == 2) {
            return true;
        }
        return false;
    }

    public function cancel($sNopeg) {
        $this->status = 4;
        $this->modified_by = $sNopeg;
        $this->save();
        return array("status" => 2, "msg" => "Success, now Manual Attandances status are " . $this->getStatusText());
    }

    public static function getProposeMessage(TMSValidate $oTMSValidate, $sNama = "", $sNamaSup = "") {
        if (empty($sNama)) {
            $sNama = LibStaticContent::getEmpNama($oTMSValidate->nopeg);
        }
        if (empty($sNamaSup)) {
            $sNamaSup = LibStaticContent::getEmpNama($oTMSValidate->superior);
        }
        $oTMSEmpData=  TMSEmpData::where('data_id',$oTMSValidate->data_id)->first();
        $sRtn = "<div style=\"font-family:Calibri,Arial,Tahoma;font-size: 90%;overflow: auto;padding: 0.5em;\">";
        $sRtn .= "Yth. Bapak/Ibu " . ucwords(strtolower($sNamaSup)) . ",<br /><br />";
        $sMsg = "<p>Sehubungan dengan permohonan Manual Attandance oleh Bapak/Ibu  " . $sNama . "(".$oTMSValidate->nopeg."), pada tanggal " . $oTMSEmpData->getTanggalFormatDMY() ." dikarenakan \"" . $oTMSValidate->keterangan . "\".</p>";
        $sRtn .= $sMsg . "";
        $sRtn .= "<br />";
        $sRtn .= "<br />";
        $sRtn .= "Demikian atas perhatiannya disampaikan terima kasih.<br />";
        $sRtn .= "<br />";
        $sRtn .= "<br />";
        $sRtn .= "Hormat Kami,<br/>";
        $sRtn .= "Human Capital Management";
        $sRtn .= "</div>";
        $ret = array();
        $ret['res'] = 1;
        $ret['subject_email'] = 'Manual Attandance need Appraisal';
        $ret['subject_gcm'] = 'Manual Attandance need Appraisal';
        $ret['msg_email'] = $sRtn;
        $ret['msg_gcm'] = "Manual Attandance Propose ," .$oTMSValidate->nopeg."/" . ucwords(strtolower($sNama)) . " (" . $oTMSEmpData->getTanggalFormatDMY() . ") " . $oTMSValidate->keterangan;
        $ret['nopeg'] = $oTMSValidate->superior;
        return $ret;
    }
    
    public static function getCanceledMessage(TMSValidate $oTMSValidate, $sNama = "") {
        if (empty($sNama)) {
            $sNama = LibStaticContent::getEmpNama($oTMSValidate->nopeg);
        }
        $oTMSEmpData=  TMSEmpData::where('data_id',$oTMSValidate->data_id)->first();
        $sRtn = "<div style=\"font-family:Calibri,Arial,Tahoma;font-size: 90%;overflow: auto;padding: 0.5em;\">";
        $sRtn .= "Yth. Bapak/Ibu " . ucwords(strtolower($sNama)) . ",<br /><br />";
        $sMsg = "<p>Sehubungan dengan permohonan Manual Attandance " . $oTMSValidate->getLeaveTypeDesc() . " pada tanggal " . $oTMSEmpData->getTanggalFormatDMY() . " dikarenakan \"" . $oTMSValidate->keterangan. "\" Telah dicancel.</p>";
        $sRtn .= $sMsg . "";
        $sRtn .= "<br />";
        $sRtn .= "<br />";
        $sRtn .= "Demikian atas perhatiannya disampaikan terima kasih.<br />";
        $sRtn .= "<br />";
        $sRtn .= "<br />";
        $sRtn .= "Hormat Kami,<br/>";
        $sRtn .= "Human Capital Management";
        $sRtn .= "</div>";
        $ret = array();
        $ret['res'] = 1;
        $ret['subject_email'] = 'Manual Attandance Canceled';
        $ret['subject_gcm'] = 'Manual Attandance Canceled';
        $ret['msg_email'] = $sRtn;
        $ret['msg_gcm'] = "Manual Attandance Canceled," .$oTMSValidate->nopeg."/". ucwords(strtolower($sNama)) . " (" . $oTMSEmpData->getTanggalFormatDMY(). ") " . $oTMSValidate->keterangan;
        $ret['nopeg'] = $oTMSValidate->nopeg;
        return $ret;
    }
    
    public static function getApprovedMessage(TMSValidate $oTMSValidate, $sNama = ""){
        if (empty($sNama)) {
            $sNama = LibStaticContent::getEmpNama($oTMSValidate->nopeg);
        }
        $oTMSEmpData=  TMSEmpData::where('data_id',$oTMSValidate->data_id)->first();
        $sRtn = "<div style=\"font-family:Calibri,Arial,Tahoma;font-size: 90%;overflow: auto;padding: 0.5em;\">";
        $sRtn .= "Yth. Bapak/Ibu " . ucwords(strtolower($sNama)) . ",<br /><br />";
        $sMsg = "<p>Sehubungan dengan permohonan manual attandance pada tanggal " . $oTMSEmpData->getTanggalFormatDMY() . " dikarenakan \"" . $oTMSValidate->keterangan . "\" Telah diapprove dengan note ".$oTMSValidate->sup_reason.".</p>";
        $sRtn .= $sMsg . "";
        $sRtn .= "<br />";
        $sRtn .= "<br />";
        $sRtn .= "Demikian atas perhatiannya disampaikan terima kasih.<br />";
        $sRtn .= "<br />";
        $sRtn .= "<br />";
        $sRtn .= "Hormat Kami,<br/>";
        $sRtn .= "Human Capital Management";
        $sRtn .= "</div>";
        $ret = array();
        $ret['res'] = 1;
        $ret['subject_email'] = 'Manual Attandance Approved';
        $ret['subject_gcm'] = 'Manual Attandance Approved';
        $ret['msg_email'] = $sRtn;
        $ret['msg_gcm'] = "Manual Attandance Approved," .$oTMSValidate->nopeg."/". ucwords(strtolower($sNama)) . " (" . $oTMSEmpData->getTanggalFormatDMY(). ") " . $oTMSValidate->sup_reason;
        $ret['nopeg'] = $oTMSValidate->nopeg;
        return $ret;
    }
    
    public static function getRejectedMessage(TMSValidate $oTMSValidate, $sNama = ""){
        if (empty($sNama)) {
            $sNama = LibStaticContent::getEmpNama($oTMSValidate->nopeg);
        }
        $oTMSEmpData=  TMSEmpData::where('data_id',$oTMSValidate->data_id)->first();
        $sRtn = "<div style=\"font-family:Calibri,Arial,Tahoma;font-size: 90%;overflow: auto;padding: 0.5em;\">";
        $sRtn .= "Yth. Bapak/Ibu " . ucwords(strtolower($sNama)) . ",<br /><br />";
        $sMsg = "<p>Sehubungan dengan permohonan Manual Attandance pada tanggal " . $oTMSEmpData->getTanggalFormatDMY() . " dikarenakan \"" . $oTMSValidate->keterangan . "\" Telah ditolak dengan note ".$oTMSValidate->sup_reason."</p>";
        $sRtn .= $sMsg . "";
        $sRtn .= "<br />";
        $sRtn .= "<br />";
        $sRtn .= "Demikian atas perhatiannya disampaikan terima kasih.<br />";
        $sRtn .= "<br />";
        $sRtn .= "<br />";
        $sRtn .= "Hormat Kami,<br/>";
        $sRtn .= "Human Capital Management";
        $sRtn .= "</div>";
        $ret = array();
        $ret['res'] = 1;
        $ret['subject_email'] = 'Manual Attandance Rejected';
        $ret['subject_gcm'] = 'Manual Attandance Rejected';
        $ret['msg_email'] = $sRtn;
        $ret['msg_gcm'] = "Manual Attandance Rejected," .$oTMSLeave->nopeg."/". ucwords(strtolower($sNama)) . " (" . $oTMSEmpData->getTanggalFormatDMY(). ") " . $oTMSLeave->keterangan;
        $ret['nopeg'] = $oTMSLeave->nopeg;
        return $ret;
    }

}
