<?php

namespace App;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Librarys\LibStaticConnect;
use App\TMSEmpData;
use App\TMSMasterAbsAtt;

/**
 * Description of CoreUserRole
 *
 * @author user
 */
class TMSAbsAtt extends Model {

    //put your code here
    protected $table = 'tms_abs_att';
    protected $primaryKey = 'abs_id';

    public static function insertTMSAbsAtt(TMSEmpData $empData, $subty, $begda, $endda, $by, $toSAP = 0) {
        $oTMSAbsAtt = TMSAbsAtt::where('data_id', $empData->data_id)->where('subty', $subty)->where('start_date', $begda)->where('end_date', $endda)->first();
        if (empty($oTMSAbsAtt)) {
            $oTMSAbsAtt = new TMSAbsAtt();
            $oTMSAbsAtt->data_id = $empData->data_id;
            $oTMSAbsAtt->subty = $subty;
            $oTMSAbsAtt->start_date = $begda;
            $oTMSAbsAtt->end_date = $endda;
            $oTMSAbsAtt->modified_by = $by;
            $oTMSAbsAtt->to_sap = $toSAP;
            $oTMSAbsAtt->save();
            $empData->isAbsAtt = 1;
            $empData->modified_by = $by;
            $empData->save();
            return true;
        }
        return false;
    }
    
    public static function insertTMSAbsAtts($empDatas,$subty,$begda,$endda,$by,$toSAP=0){
        if(empty($empDatas) || count($empDatas)==0){
            return null;
        }
        foreach($empDatas as $empData){
            TMSAbsAtt::insertTMSAbsAtt($empData, $subty, $begda, $endda, $by,$toSAP);
        }
        return true;
    }
    
    public static function getSubtyText($data_id){
        $oTMSAbsAtt = TMSAbsAtt::where('data_id',$data_id)->first();
        if(empty($oTMSAbsAtt)){
            return "";
        }
        return TMSMasterAbsAtt::getSubtyText($oTMSAbsAtt->subty);
    }

}
