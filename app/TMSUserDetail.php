<?php

namespace App;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Librarys\LibStaticConnect;
use Carbon\Carbon;

/**
 * Description of CoreUserRole
 *
 * @author user
 */
class TMSUserDetail extends Model {

    //put your code here
    protected $table = 'tms_user_detail';
    protected $primaryKey = 'user_d_id';

    public static function getLakhar($sNopeg) {
        $now = Carbon::now()->toDateString();
        $oRet = TMSUserDetail::where('nopeg', $sNopeg)->where('isLakhar', 1)->where('isActive', 1)
                        ->where('sdate', '<=', $now)->where('edate', '>=', $now)->get();
        if (empty($oRet) || count($oRet) == 0) {
            return null;
        }
        return $oRet;
    }

    public static function getOrgMaintain($sNopeg, $sMntType) {
        $aRets = null;
        $now = Carbon::now()->toDateString();
        if ($sMntType == "SORD") {
            $oRets = TMSUserDetail::where('nopeg', $sNopeg)->where('isLakhar', 1)->where('isActive', 1)
                            ->where('sdate', '<=', $now)->where('edate', '>=', $now)->get();
//            $sQuery = "SELECT org_maint " .
//                    "FROM tms_user_detail d " .
//                    "WHERE nopeg = '" . $sNopeg . "' and isLakhar=1 AND isActive=1 AND CURDATE() BETWEEN sdate AND edate";
        } else if ($sMntType == "TADM") {
            $oRets = TMSUserDetail::where('nopeg', $sNopeg)->where('isLakhar', 0)->where('isActive', 1)
                            ->where('sdate', '<=', $now)->where('edate', '>=', $now)->get();
//            $sQuery = "SELECT org_maint " .
//                    "FROM tms_user_detail d " .
//                    "WHERE nopeg = '" . $sNopeg . "' and isLakhar=0 AND isActive=1 AND CURDATE() BETWEEN sdate AND edate";
        } else if ($sMntType == "ALL") {
            $oRets = TMSUserDetail::where('nopeg', $sNopeg)->where('isActive', 1)
                            ->where('sdate', '<=', $now)->where('edate', '>=', $now)->get();
//            $sQuery = "SELECT org_maint " .
//                    "FROM tms_user_detail d " .
//                    "WHERE nopeg = '" . $sNopeg . "' AND isActive=1 AND CURDATE() BETWEEN sdate AND edate";
        }
        if(!empty($oRets) && count($oRets)>0){
            foreach ($oRets as $oRet){
                $aRet[]=$oRet->org_maint;
            }
        }
        
        return $aRets;

//        $aRun = $this->db->query($sQuery);
//        $aRst = $aRun->result();
//
//        $aRun->free_result();
//        return $aRst;
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	    public function getNama(){
        return $this->getDataNama("nama");
    }
    public function getDataNama(){
        if(empty($this->aEmp)){
            $this->aEmp = LibStaticContent::getEmpNama($this->nopeg_maintain);
        }
        
        if(empty($this->aEmp)){
            return "Nama";
        }
        return $this->aEmp;    
    }

}
