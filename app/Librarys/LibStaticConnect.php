<?php

namespace App\Librarys;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Cache;
use Validator;
use Sentinel;
use Response;
use DB;
use Carbon\Carbon;
use Ixudra\Curl\Facades\Curl;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Description of LibStaticContent
 *
 * @author user
 */
class LibStaticConnect {

    public static function getConnectUrl() {
        return "http://192.168.30.40/hconline/module/connect";
    }

    public static function get_org_all($sOrg) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/employee/get_all_org/" . $sOrg;
        $oResult = Curl::to($sInput)
                ->asJson()
                ->get();
        LibStaticConnect::connectLog($sInput, json_encode($oResult));
        if (!empty($oResult)) {
            return $oResult;
        }
        return null;
    }

    public static function get_org_down($sOrg, $sDefConfigSub) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/organization/tms_get_org_down/" . $sOrg . "/" . $sDefConfigSub;
        $oResult = Curl::to($sInput)
                ->asJson()
                ->get();
        LibStaticConnect::connectLog($sInput, json_encode($oResult));
        if (!empty($oResult)) {
            return $oResult;
        }
        return null;
    }

    public static function getOrgDown($sOrg, $sDefConfigSub=5) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/organization/tms_get_org_down/" . $sOrg . "/" . $sDefConfigSub;
        $oResult = Curl::to($sInput)
                ->asJson()
                ->get();
        LibStaticConnect::connectLog($sInput, json_encode($oResult));
        if (!empty($oResult)) {
            return $oResult;
        }
        return null;
    }

    public static function getOrgDesc($sOrg) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/organization/get_desc_org/" . $sOrg;
        $oResult = Curl::to($sInput)
                ->asJson()
                ->get();
        LibStaticConnect::connectLog($sInput, json_encode($oResult));
        if (!empty($oResult)) {
            return $oResult;
        }
        return null;
    }

    public static function getSubordinateFromOrg($sOrg, $sDefConfigSub = "5") {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/employee/get_subordinates_from_org/" . $sOrg . "/" . $sDefConfigSub;
        $oResult = Curl::to($sInput)
                ->asJson()
                ->get();
        LibStaticConnect::connectLog($sInput, json_encode($oResult));
        if (!empty($oResult)) {
            return $oResult;
        }
        return null;
//        $sEmps = $this->curl->simple_get(URL_CURL . '/employee/get_subordinates_from_org/' . $sOrg . '/' . $sDefConfifSub);
//        $oEmp = json_decode($sEmps);
//
//        return $oEmp;
    }

    public static function getEmpFromSingleOrg($sOrg) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/organization/get_emp_org/" . $sOrg ;
        $oResult = Curl::to($sInput)
                ->asJson()
                ->get();
        LibStaticConnect::connectLog($sInput, json_encode($oResult));
        if (!empty($oResult)) {
            return $oResult;
        }
        return null;
    }

    public static function getArrOrg($sNopeg) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/employee/get_arr_org/" . $sNopeg;
        $oResult = Curl::to($sInput)
                ->asJson()
                ->get();
        LibStaticConnect::connectLog($sInput, json_encode($oResult));
        if (!empty($oResult)) {
            return $oResult;
        }
        return null;
    }

    public static function getArrPosition($sNopeg) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/employee/get_arr_position/" . $sNopeg;
        $oResult = Curl::to($sInput)
                ->asJson()
                ->get();
        LibStaticConnect::connectLog($sInput, json_encode($oResult));
        if (!empty($oResult)) {
            return $oResult;
        }
        return null;
    }

    public static function cekPejabat($sNopeg) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/employee/is_manage_org/" . $sNopeg;
        $oResult = Curl::to($sInput)
                ->asJson()
                ->get();
        LibStaticConnect::connectLog($sInput, json_encode($oResult));
        if (!empty($oResult)) {
            return $oResult;
        }
        return null;
    }

    public static function getSubordinate($sNopeg, $sDefConfigSub = "5") {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/employee/get_subordinates/" . $sNopeg . "/" . $sDefConfigSub;
        $oResult = Curl::to($sInput)
                ->asJson()
                ->get();
        LibStaticConnect::connectLog($sInput, json_encode($oResult));
        if (!empty($oResult)) {
            return $oResult;
        }
        return null;
    }

    public static function getEmpData($nopeg) {
//        var_dump($nopeg);exit;
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/employee/get_all_master_emp/" . $nopeg;
        $oResult = Curl::to($sInput)
                ->asJson()
                ->get();
        LibStaticConnect::connectLog($sInput, json_encode($oResult));
        if (!empty($oResult)) {
            return $oResult;
        }
        return null;
    }

    public static function connectLog($sInput, $sOutput, $iLogType = Logger::INFO) {
        return null;
//        $view_log = new Logger('CONNECT_LOGS');
//        $sDate = date("Ymd");
//        $path = storage_path('logs/connect_' . $sDate . '.log');
//        $idLog = request('idLogReqApi', $default = -1);
//        $view_log->pushHandler(new StreamHandler($path, $iLogType));
//        $view_log->log($iLogType, $idLog . " | " . $sInput . " | " . $sOutput);
    }

    public static function sapSubstitution($nopeg, $begda, $endda, $dws) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/sap/sap_substitution";
        $fields = array(
            'nopeg' => $nopeg,
            'begda' => $begda,
            'endda' => $endda,
            'dws' => $dws,
        );
//        var_dump($fields);
        $oResult = Curl::to($sInput)
                ->withData($fields)
                ->post();
        return json_decode($oResult);
    }

    public static function sapOvertime($nopeg, $begda, $endda, $begtime,$endtime) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/sap/sap_overtime";
        $fields = array(
            'nopeg' => $nopeg,
            'begda' => $begda,
            'endda' => $endda,
            'begtime' => $begtime,
            'endtime' => $endtime,
        );
        $oResult = Curl::to($sInput)
                ->withData($fields)
                ->post();
        return json_decode($oResult);
    }

    public static function get_tms_emp_check($aNopeg, $aFilter) {
        $fields = array(
            'nopegs' => $aNopeg,
            'filters' => $aFilter
        );
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/tms/emp_user_check";
        $oResult = Curl::to($sInput)
                ->withData($fields)
                ->post();
        return json_decode($oResult);
    }

    public static function get_sap_pwt() {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/sap/get_pwt";
        $oResult = Curl::to($sInput)
                ->post();
        return json_decode($oResult);
    }

    public static function get_sap_pwt_date($yyyymmdd) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/sap/get_pwt";
        $oResult = Curl::to($sInput)
                ->withData(array('date' => $yyyymmdd))
                ->post();
        return json_decode($oResult);
    }

    public static function get_sap_pwt_by_nopeg_ge($nopeg, $dDate = null) {
        if (empty($dDate)) {
            $dDate = LibStaticContent::getStartDateLimitEmpData();
        }
        $fields = array(
            'nopeg' => $nopeg,
            'date' => $dDate->format('Ymd')
        );
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/sap/get_pwt_by_nopeg_ge";
        $oResult = Curl::to($sInput)
                ->withData($fields)
                ->post();
        return json_decode($oResult);
    }

    public static function get_sap_leave_yesterday() {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/sap/get_leave_by_date";
        $oResult = Curl::to($sInput)
                ->post();
        return json_decode($oResult);
    }

    public static function get_sap_leave_date($yyyymmdd) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/sap/get_leave_by_date";
        $oResult = Curl::to($sInput)
                ->withData(array('date' => $yyyymmdd))
                ->post();
        return json_decode($oResult);
    }

    public static function get_sap_absence_yesterday() {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/sap/get_absence_by_date";
        $oResult = Curl::to($sInput)
                ->post();
        return json_decode($oResult);
    }

    public static function get_sap_absence_date($yyyymmdd) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/sap/get_absence_by_date";
        $oResult = Curl::to($sInput)
                ->withData(array('date' => $yyyymmdd))
                ->post();
        return json_decode($oResult);
    }

    public static function get_sap_substitution_yesterday() {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/sap/get_substitution_by_date";
        $oResult = Curl::to($sInput)
                ->post();
        return json_decode($oResult);
    }

    public static function get_sap_substitution_date($yyyymmdd) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/sap/get_substitution_by_date";
        $oResult = Curl::to($sInput)
                ->withData(array('date' => $yyyymmdd))
                ->post();
        return json_decode($oResult);
    }

    public static function get_sap_overtime_yesterday() {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/sap/get_overtime_by_date";
        $oResult = Curl::to($sInput)
                ->post();
        return json_decode($oResult);
    }

    public static function get_sap_overtime_date($yyyymmdd) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/sap/get_overtime_by_date";
        $oResult = Curl::to($sInput)
                ->withData(array('date' => $yyyymmdd))
                ->post();
        return json_decode($oResult);
    }

    public static function getWhoManage($sNopeg, $sOrgUnit) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/employee/get_who_manage/" . $sNopeg . "/" . $sOrgUnit;
        $oResult = Curl::to($sInput)
                ->post();
        return json_decode($oResult);
    }

    public static function getSuperior($sNopeg) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/employee/get_superior/" . $sNopeg;
        $oResult = Curl::to($sInput)
                ->post();
        return json_decode($oResult);
    }

    public static function sendEmail($to, $cc_email, $subject, $message) {
        $fields = array(
            'to' => $to,
            'cc' => $cc_email,
            'subject' => $subject,
            'message' => $message,
            'otype' => 'tms3'
        );
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/mail/store_to_outbox";
        $sResult = Curl::to($sInput)
                ->withData($fields)
                ->post();
        return $sResult;
    }

    public static function getEmail($sNopeg) {
        if (empty($sNopeg))
            return "";
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/hcis/get_email/" . $sNopeg;
        $sCurl = Curl::to($sInput)
                ->post();
        if (!empty($sCurl)) {
            return $sCurl . "@garuda-indonesia.com";
        }
        return "";
    }

    public static function sendPushNotifHCMobile($nopeg, $subject, $msg) {
        $fields = array(
            'nopeg' => $nopeg,
            'subject' => $subject,
            'message' => $msg,
            'app_module' => 'tms3'
        );
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/gcm/store_to_preoutbox";
        $sResult = Curl::to($sInput)
                ->withData($fields)
                ->post();
        return $sResult;
    }

    public static function getAbsQuota($nopeg, $sYear) {
        $fields = array(
            'nopeg' => $nopeg,
            'year' => $sYear
        );
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/sap/getAbsQuota";
        $oResult = Curl::to($sInput)
                ->withData($fields)
                ->post();
        if($oResult=="-")return null;
        return json_decode($oResult);
    }

    public static function getCsQuota($nopeg, $sYear) {
        $fields = array(
            'nopeg' => $nopeg,
            'year' => $sYear
        );
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/sap/getCsQuota";
        $oResult = Curl::to($sInput)
                ->withData($fields)
                ->post();
        if($oResult=="-")return null;
        return json_decode($oResult);
    }

    public static function get_tms_emps_info($sNopegs) {
        $fields = array(
            'nopegs' => $sNopegs
        );
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/tms/get_emps_info";
        $oResult = Curl::to($sInput)
                ->withData($fields)
                ->post();
        return json_decode($oResult);
    }
    public static function getHolderPositionFromOrg($sOrg) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/master_relation/get_holder_position_from_org/" . $sOrg;
        $oResult = Curl::to($sInput)
                ->post();
        return json_decode($oResult);
    }
    public static function getJobFromPositon($sPosition) {
        $sInput = LibStaticConnect::getConnectUrl() . "/index.php/master_relation/get_job_from_position/" . $sPosition;
        $oResult = Curl::to($sInput)
                ->post();
//        var_dump($oResult);exit;
        return json_decode($oResult);
    }

}
