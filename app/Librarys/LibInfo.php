<?php

namespace App\Librarys;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 
use App\Models\PAEmpOrg;
use App\Models\PAEmpContract;
use App\Models\PAEmpData;
use Carbon\Carbon;

/**
 * Description of LibInfo
 *
 * @author user
 */
class LibInfo {

    public $full_name = "";
    public $nik = "";
    public $emp_grp = "";
    public $emp_sgrp = "";
    public $project = "";
    public $sector = "";
    public $org_unit = "";
    public $department = "";
    public $position = "";
    public $legacy_id = "";
   
    public function __construct($iEid) {
        $now = Carbon::now();
                
        $oEmpData = PAEmpData::where('nik', $iEid)
                ->where('begda', '<=', $now->toDateString())
                ->where('endda', '>=', $now->toDateString())
                ->first();
        if($oEmpData){
            $this->full_name = $oEmpData->full_name;
            $this->nik = $oEmpData->nik;
        }else{
            $this->name = "";
            $this->nik = "";
        }
        
        $oEmpOrgs = PAEmpOrg::where('nik', $iEid)
                ->where('begda', '<=', $now->toDateString())
                ->where('endda', '>=', $now->toDateString())
                ->first();
        if($oEmpOrgs){
            $this->emp_grp = $oEmpOrgs->emp_grp;
            $this->emp_sgrp = $oEmpOrgs->emp_sgrp;
            $this->project = $oEmpOrgs->project;
            $this->sector = $oEmpOrgs->sector;
            $this->department = $oEmpOrgs->department;
            $this->position = $oEmpOrgs->position;
            $this->org_unit = $oEmpOrgs->org_unit;
        }else{
            $this->emp_grp = "";
            $this->emp_sgrp = "";
            $this->project = "";
            $this->sector = "";
            $this->department = "";
            $this->position = "";
            $this->org_unit = "";
        }
        
        $oEmpContract = PAEmpContract::where('nik', $iEid)
                ->where('begda', '<=', $now->toDateString())
                ->where('endda', '>=', $now->toDateString())
                ->first();
        if($oEmpContract){
            $this->legacy_id = $oEmpContract->legacy_id;
        }else{
            $this->legacy_id = "";
        }
        
        return false;
    }

}
