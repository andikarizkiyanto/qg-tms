<?php

namespace App\Librarys;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//use App\Models\CoreModule;
//use App\Models\CoreSubModule;
//use App\Models\CoreRole;
//use App\Models\CoreUserRole;
//use App\Models\CoreMaintPriv;
//use App\Models\CoreConfig;
//use App\Models\CoreUserMapMaint;

use App\TMSEmpFast;
use Sentinel;
use Cache;
use DB;
use Carbon\Carbon;
use App\Librarys\LibStaticConnect;
use App\TMSMasterOutsource;
use App\User;
use Validator;
use App\Librarys\LibStaticTMS;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use App\TMSEmpData;
use App\TMSMasterHoliday;
use App\TMSAbsAtt;
use PHPExcel_Style_Border;
use App\TMSNopegMaintain;
use Auth;
/**
 * Description of LibStaticContent
 *
 * @author user
 */
class LibStaticContent {

    //put your code here

    public static function genSheetSingleNopegSingleMonth(LaravelExcelWorksheet $sheet, $nopeg, $carbon) {
        $oEmp = LibStaticContent::getDataSessionEmp($nopeg);

        $tms = TMSEmpData::where('nopeg', $nopeg)
                        ->whereMonth('tanggal', '=', $carbon->format('m'))->whereYear('tanggal', '=', $carbon->format('Y'))
                        ->orderBy('tanggal', 'desc')->get();
        $aKey = array();
        $aReason = array();
        if (!empty($tms) && count($tms) > 0) {
            foreach ($tms as $data) {
                $aKey[$data->dws] = $data->getDWSTable();
                $aReason[$data->data_id] = $data->getReasonText();
            }
        }

        $sheet->setCellValue('A1', $carbon->format('F Y'));
        $sheet->setCellValue('C1', "Nopeg");
        $sheet->setCellValue('F1', "Position");
        $sheet->setCellValue('C2', "Name");
        $sheet->setCellValue('F2', "Unit");

        $sheet->setCellValue('D1', ": " . $oEmp['nopeg']);
        $sheet->setCellValue('G1', ": " . $oEmp['jabatan']);
        $sheet->setCellValue('D2', ": " . $oEmp['fullname']);
        $sheet->setCellValue('G2', ": " . $oEmp['unit']);
        $sheet->getStyle('A1:G2')->getFont()->setBold(true);

        $sheet->setCellValue('A5', 'Numb.');
        $sheet->setCellValue('B5', 'Day');
        $sheet->setCellValue('C5', 'Date');
        $sheet->setCellValue('D5', 'Code');
        $sheet->setCellValue('E5', 'Working Sched (IN)');
        $sheet->setCellValue('F5', 'Working Sched (OUT)');
        $sheet->setCellValue('G5', 'Clock In');
        $sheet->setCellValue('H5', 'Clock Out');
        $sheet->setCellValue('I5', 'Reason');
        $sheet->getStyle('A5:I5')->getFont()->setBold(true);
        $row = 5;
        $i = 0;
        foreach ($tms as $oTms) {
            $row++;
            $nowCarbon = Carbon::createFromFormat("Y-m-d", $oTms->tanggal);
            $sheet->setCellValue('A' . $row, ($i + 1));
            $sheet->setCellValue('B' . $row, $nowCarbon->format('l'));
            $sheet->setCellValue('C' . $row, $nowCarbon->format("d.m.Y"));
            $sheet->setCellValue('D' . $row, $oTms->dws);
            $sheet->setCellValue('E' . $row, $aKey[$oTms->dws]->jam_masuk);
            $sheet->setCellValue('F' . $row, $aKey[$oTms->dws]->jam_keluar);
            $sheet->setCellValue('G' . $row, $oTms->clock_in);
            $sheet->setCellValue('H' . $row, $oTms->clock_out);
            $sStat = "";
            if (TMSMasterHoliday::isHoliday($oTms->tanggal)) {
                $sStat = TMSMasterHoliday::getKeteranganHoliday($oTms->tanggal);
            }
            if (!empty($oTms->isSubstitutions) && $oTms->isSubstitutions == 1) {
                $sStat.=" | Substitusi";
            }
            if (!empty($oTms->isValidate) && $oTms->isValidate == 1) {
                $sStat.=" | Hadir Manual";
            }
            if (!empty($oTms->isOvertime) && $oTms->isOvertime == 1) {
                $sStat.=" | Overtime";
            }
            if (!empty($oTms->isAbsAtt) && $oTms->isAbsAtt == 1) {
                $l = TMSAbsAtt::getSubtyText($oTms->data_id);
                if ($l) {
                    $sStat.=" | " . $l;
                }
            }

            $sheet->setCellValue('I' . $row, $sStat);
        }
        $sheet->getStyle('A5:I' . $row)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        return $sheet;
    }

    public static function getMonthDesc($i) {
        $aMonth = array(1 => "January", 2 => "February", 3 => "March", 4 => "April", 5 => "May", 6 => "June", 7 => "July", 8 => "August", 9 => "September", 10 => "October", 11 => "November", 12 => "December");
        if (empty($aMonth[$i])) {
            return "";
        }
        return $aMonth[$i];
    }

    public static function in_2char($int) {
        if ($int < 10) {
            return "0" . $int;
        }
        return $int;
    }

    public static function getDateLimitEmpData() {
        return Carbon::createFromDate(2016, 12, 31);
    }

    public static function getStartDateLimitEmpData() {
        return Carbon::createFromDate(2016, 1, 1);
    }

    public static function getStartFlexiDate() {
        $oCarbon = Carbon::createFromDate(2016, 9, 1);
        //LibStaticTMS::tmsLog("getStartFlexiDate", "|" . $oCarbon . "|");
        return $oCarbon;
    }

    public static function getArrayExcludePWT() {
        return array('CREW');
    }

    public static function getArrayNopegFlexi() {
        return Cache::remember("NOPEG_FLEXI", 5, function() {
            return \App\Models\TMSFlexiCriteria::generateNopeg();
        });
//        return array('519457', '522241', '523639', '524237', '524595', '524827', '524856', '525012', '525101', '525265', '526569', '526641', '528367', '528424', '528502', '528976', '530224', '530228', '530418', '530434', '530639', '530734', '532265', '532285', '532935', '533037', '533233', '533234', '533238', '533669', '533670', '533671', '533720', '533722', '533762', '533772', '533897', '533899', '533900', '535789', '535791', '535792', '537448', '538825', '539490', '539810', '540094', '540097', '540135', '540310', '540341', '540342', '540343', '540344', '540345', '540346', '540347', '540348', '540349', '540350', '540351', '540352', '540353', '540354', '540355', '540356', '540357', '540358', '540359', '540360', '540361', '540369', '540374', '540375', '540376', '540377', '540378', '540379', '540380', '540381', '540382', '540383', '540384', '540463', '540464', '540475');
    }

    public static function getNopegSuperior($sNopeg) {
//        return "533238";
        $sOrg = "";
        $now = Carbon::now();
        $bOsrc = Auth::user()->getSessionVariabel('isOutsource');
        $oTMSEmpFast = TMSEmpFast::where('nopeg', $sNopeg)->where('start_date', '>=', $now->toDateString())->where('end_date', '<=', $now->toDateString())->first();
        if (!empty($oTMSEmpFast)) {
            $sOrg = $oTMSEmpFast->org_unit;
        } else if ($bOsrc) {
            $sOrg = Auth::user()->getSessionVariabel('org_unit');
        }
        if ($sOrg <> "" || $bOsrc) {
            $oEmp = LibStaticConnect::getWhoManage($sNopeg, $sOrg);
        } else {
            $oEmp = LibStaticConnect::getSuperior($sNopeg);
        }
        if (!empty($oEmp) && !empty($oEmp->nopeg)) {
            return $oEmp->nopeg;
        }
    }

    public static function getEmpNama($sNopeg) {
//        return "VICKY FIRMANSYAH";
        $user = User::where('email', $sNopeg)->first();
        if (empty($user)) {
            return "";
        }
        if ($user->isOutsource == 1) {
            $oEmp = TMSMasterOutsource::getEmpInfo($sNopeg);
            return $oEmp->nama;
        } else {
            $oEmp = LibStaticConnect::getEmpData($sNopeg);
            return $oEmp->nama;
        }
        return "";
    }

    public static function getDataSessionEmp($sNopeg) {
        $oEmp = LibStaticConnect::getEmpData($sNopeg);
        $oPos = LibStaticConnect::getArrPosition($sNopeg);
        $oOrg = LibStaticConnect::getArrOrg($sNopeg);
        $oRst['nopeg'] = $sNopeg;
        $oRst['fullname'] = $oEmp->nama;
        $oRst['nama'] = ucwords(strtolower($oEmp->nama));
        $oRst['jabatan'] = $oPos->position_name;
        $oRst['unit'] = $oOrg->short;
        $oRst['org_unit'] = $oEmp->org_unit;
        $oRst['job_key'] = $oEmp->job_key;
        $oRst['ps_group'] = $oEmp->PS_group;
        $oRst['ee_subgrp'] = $oEmp->EE_subgrp;
        return $oRst;
    }

    public static function getDataSessionOutsource($sNopeg) {
        $oEmp = TMSMasterOutsource::getEmpInfo($sNopeg);
        if (empty($oEmp))
            return null;
        // Outsource
        $temp = LibStaticConnect::getOrgDesc($oEmp->org_unit);
        $oRst['nopeg'] = $sNopeg;
        $oRst['fullname'] = $oEmp->nama;
        $oRst['nama'] = ucwords(strtolower($oEmp->nama));
        $oRst['jabatan'] = $oEmp->jabatan;
        if (!empty($temp))
            $oRst['unit'] = $temp->SHORT;
        else
            $oRst['unit'] = "-";
        $oRst['org_unit'] = $oEmp->org_unit;
        $oRst['job_key'] = '99999999';
        $oRst['ps_group'] = 'D';
        return $oRst;
    }

    public static function genValidation($aPost, $aValidParam, $customMessage = array()) {
        $sMessage = "";
        $validator = Validator::make($aPost, $aValidParam, $customMessage);
        if ($validator->fails()) {
            $messages = $validator->errors();
            foreach ($messages->all() as $message) {
                $sMessage.=$message . "<br/>";
            }
        }
        return $sMessage;
    }

    public static function getLeaveTypeDesc($leave_type) {
        $oLeaveTypes = Cache::remember("LEAVE_LIST", 60, function() {
                    return DB::table('tms_master_abs_att')->get();
                });
        foreach ($oLeaveTypes as $oTypes) {
            if ($oTypes->subty == $leave_type) {
                return $oTypes->text;
            }
        }
        return "-";
    }

    public static function getLeaveList() {
//        Cache::pull("LEAVE_LIST");
        $oLeaveTypes = Cache::remember("LEAVE_LIST", 60, function() {
                    return \App\Models\TMSMasterAbsAtt::get();
                });
        $aRet = array();
        foreach ($oLeaveTypes as $oTypes) {
            if ($oTypes->isShow == 1) {
                $aRet[] = $oTypes;
            }
        }
        return $aRet;
    }

    public static function getStatusTextType2($iStatus) {
        $oStatus = Cache::remember('ABBRV_2', 60, function() {
                    return DB::table('tms_master_abbrv')->where('type', '2')->get();
                });
        if (empty($oStatus) || empty($iStatus)) {
            return "";
        } else {
            foreach ($oStatus as $row) {
                if ($row->short == $iStatus) {
                    return $row->text;
                }
            }
            return "";
        }
    }

    public static function getStatusTextType1($iStatus) {
        $oStatus = Cache::remember('ABBRV_1', 60, function() {
                    return DB::table('tms_master_abbrv')->where('type', '1')->get();
                });
        if (empty($oStatus) || empty($iStatus)) {
            return "";
        } else {
            foreach ($oStatus as $row) {
                if ($row->short == $iStatus) {
                    return $row->text;
                }
            }
            return "";
        }
    }

    public static function getEmpInfo($nopeg) {
        //contain : nopeg | nama | position | unit | stext
        return Cache::get('EMPINFO' . $nopeg, null);
    }

    public static function storeEmpInfo($nopeg, $aData) {
        Cache::put('EMPINFO' . $nopeg, $aData, 720);
    }

    public static function getEmpInfoCache($nopeg) {
        $aRet = LibStaticContent::getEmpInfo($nopeg);
        if (!empty($aRet)) {
            return $aRet;
        } else {
            LibStaticContent::storeEmpInfoCache($nopeg);
            $aRet = LibStaticContent::getEmpInfo($nopeg);
        }
        return $aRet;
    }

    public static function storeEmpInfoCache($nopeg) {
        $aEmps = LibStaticConnect::get_tms_emps_info($nopeg);
        if (!empty($aEmps)) {
            foreach ($aEmps as $aEmp) {
                $temp = LibStaticContent::getEmpInfo($aEmp->nopeg);
                if (empty($temp)) {
                    LibStaticContent::storeEmpInfo($aEmp->nopeg, $aEmp);
                }
            }
        }
    }

    public static function getEmpInfoNama($nopeg) {
        $aVar = LibStaticContent::getEmpInfoCache($nopeg);
        if (empty($aVar)) {
            return "";
        }
        return $aVar->nama;
    }

    public static function genEmpInfo($nopeg) {
        $oNopegMaintains = TMSNopegMaintain::select('nopeg_maintain')->where('nopeg', $nopeg)->groupBy('nopeg_maintain')->get();
        $aNopegNeedData = array();
        foreach ($oNopegMaintains as $oNopegMaintain) {
            $temp = LibStaticContent::getEmpInfo($oNopegMaintain->nopeg_maintain);
            if (empty($temp)) {
                $aNopegNeedData[] = $oNopegMaintain->nopeg_maintain;
            }
        }
        if (!empty($aNopegNeedData)) {
            $sNopegs = implode(",", $aNopegNeedData);
            $aEmps = LibStaticConnect::get_tms_emps_info($sNopegs);
            if (!empty($aEmps)) {
                foreach ($aEmps as $aEmp) {
                    $temp = LibStaticContent::getEmpInfo($aEmp->nopeg);
                    if (empty($temp)) {
                        LibStaticContent::storeEmpInfo($aEmp->nopeg, $aEmp);
                    }
                }
            }
            //send to connect to retrieve data
            //retrieve
            //looping and save to cache
        }
    }

    public static function getDefaultEmpInfo() {
        $aTemp = array("nama" => "-", "position_name" => "-", "short" => "-", "stext" => "-");
        $sTemp = json_encode($aTemp);
        return json_decode($sTemp);
    }

//    public static function getMedDistance() {
//        return Cache::remember('MED_DISTANCE', 15, function() {
//                    $oMin = DB::table('tmh_abbrev')->where('abbrev_name', 'MED_DISTANCE')->first();
//                    return $oMin->abbrev_value;
//                });
//    }

    public static function getDataCountDWSPeriod($sNopegs, $sBegda, $sEndda) {
        $oTemp = TMSEmpData::select('dws', DB::raw('count(dws) n'))->where('dws', '<>', 'OFF')->whereIn('nopeg', explode(",", $sNopegs))->where('tanggal', '>=', $sBegda)->where('tanggal', '<=', $sEndda)->groupBy('dws')->get();
        Carbon::now();
        $aRet = array("07:00:00" => 0, "07:30:00" => 0, "08:00:00" => 0, "08:30:00" => 0, "09:00:00" => 0);
        foreach ($oTemp as $dws) {
            $res = \App\Models\TMSMasterDWS::getDWS(trim($dws->dws));
            $aRet[$res->jam_masuk] = $aRet[$res->jam_masuk] + $dws->n;
        }
        return $aRet;
    }

    public static function getDataCountJamKerjaDWS($sNopegs, $sBegda, $sEndda) {
        $oTemp = TMSEmpData::select('dws', 'clock_in', 'clock_out', DB::raw('clock_out-clock_in clock_diff'))->where('dws', '<>', 'OFF')->whereIn('nopeg', explode(",", $sNopegs))->where('tanggal', '>=', $sBegda)->where('tanggal', '<=', $sEndda)->get();
        Carbon::now();
        $aRet = array("<_6" => 0, "6_7" => 0, "7_8" => 0, "8_9" => 0, "9_10" => 0, "10_11" => 0, "11_>" => 0);
        foreach ($oTemp as $dws) {
            $res = \App\Models\TMSMasterDWS::getDWS(trim($dws->dws));
            if (($dws->clock_diff - $res->rehat) < 6) {
                $aRet["<_6"] = $aRet["<_6"] + 1;
            } else if (($dws->clock_diff - $res->rehat) < 7) {
                $aRet["6_7"] = $aRet["6_7"] + 1;
            } else if (($dws->clock_diff - $res->rehat) < 8) {
                $aRet["7_8"] = $aRet["7_8"] + 1;
            } else if (($dws->clock_diff - $res->rehat) < 9) {
                $aRet["8_9"] = $aRet["8_9"] + 1;
            } else if (($dws->clock_diff - $res->rehat) < 10) {
                $aRet["9_10"] = $aRet["9_10"] + 1;
            } else if (($dws->clock_diff - $res->rehat) < 11) {
                $aRet["10_11"] = $aRet["10_11"] + 1;
            } else {
                $aRet["11_>"] = $aRet["11_>"] + 1;
            }
        }
        return $aRet;
    }

    public static function getTopTenLowerHigher($sNopegs, $sBegda, $sEndda) {
        $aNopegs = explode(",", $sNopegs);
        $aRetx = array();
        $aRet = array();
        foreach ($aNopegs as $nopeg) {
            $aTemp = TMSEmpData::getAverageTimeDataPeriod($nopeg, $sBegda, $sEndda);
            $aRetx[$aTemp['avg_second']] = $aTemp;
        }
        krsort($aRetx);
        foreach ($aRetx as $aTemp) {
            $aRet[] = $aTemp;
        }
        return $aRet;
    }

    public static function getTopTenDenda($sNopegs, $sBegda, $sEndda) {
        $aNopegs = explode(",", $sNopegs);
        $aRetx = array();
        $aRet = array();
        foreach ($aNopegs as $nopeg) {
            $aTemp = TMSEmpData::getTotalDendaPeriod($nopeg, $sBegda, $sEndda);
            $aRetx[$aTemp['second_late']] = $aTemp;
        }
        krsort($aRetx);
        foreach ($aRetx as $aTemp) {
            $aRet[] = $aTemp;
        }
        return $aRet;
    }

    public static function getTopTenLate($sNopegs, $sBegda, $sEndda) {
        $aNopegs = explode(",", $sNopegs);
        $aRetx = array();
        $aRet = array();
        foreach ($aNopegs as $nopeg) {
            $aTemp = TMSEmpData::getTotalCameLatePeriod($nopeg, $sBegda, $sEndda);
            if ($aTemp['n_late'] > 0) {
                $aRetx[$aTemp['n_late']] = $aTemp;
            }
        }
        krsort($aRetx);
        foreach ($aRetx as $aTemp) {
            $aRet[] = $aTemp;
        }
        return $aRet;
    }

    public static function topTenSorting($aVal, $aRet = array()) {
        if (empty($aRet)) {
            $aRet[0] = $aVal;
            return $aRet;
        }
        for ($i = 0; $i < count($aRet); $i++) {
            if ($aVal['avg_second'] > $aRet[$i]['avg_second']) {
                array_splice($aRet, $i, 0, $aVal);
                return $aRet;
            }
        }
        $aRet[count($aRet)] = $aVal;
        return $aRet;
    }

    public static function secondToTimeFormat($second) {
        $hourX = floor($second) / 3600;
        $minuteX = floor($second / 60 % 60);
        $secondX = floor($second % 60);
        return sprintf('%02d:%02d:%02d', $hourX, $minuteX, $secondX);
    }

}
