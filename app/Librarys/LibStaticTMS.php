<?php

namespace App\Librarys;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Cache;
use Validator;
use Sentinel;
use Response;
use DB;
use Carbon\Carbon;
use Ixudra\Curl\Facades\Curl;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Description of LibStaticContent
 *
 * @author user
 */
class LibStaticTMS {

    public static function getTMSUrl() {
        return "http://intra.garuda-indonesia.com/hconline/tms_new";
    }

    public static function setDWS($nopeg, $data_id, $dws) {
        $sInput = LibStaticTMS::getTMSUrl() . "/index.php/web_service/setDWS";
        $fields = array(
            'nopeg' => $nopeg,
            'data_id' => $data_id,
            'dws' => $dws,
        );
        $oResult = Curl::to($sInput)
                ->withData($fields)
                ->post();
        //LibStaticTMS::tmsLog($sInput."#".json_encode($fields), json_encode($oResult));
        return $oResult;
    }

    public static function setDWSByTanggal($nopeg, $tanggal, $dws) {
        $sInput = LibStaticTMS::getTMSUrl() . "/index.php/web_service/setDWSByTanggal";
        $fields = array(
            'nopeg' => $nopeg,
            'tanggal' => $tanggal,
            'dws' => $dws,
        );
        $oResult = Curl::to($sInput)
                ->withData($fields)
                ->post();
        //LibStaticTMS::tmsLog($sInput."#".json_encode($fields), json_encode($oResult));
        return $oResult;
    }
    
    public static function tmsLog($sInput, $sOutput, $iLogType = Logger::INFO) {
//        return null;
        $view_log = new Logger('TMS_LOGS');
        $sDate = date("Ymd");
        $path = storage_path('logs/tms_' . $sDate . '.log');
        $idLog = request('idLogReqApi', $default = -1);
        $view_log->pushHandler(new StreamHandler($path, $iLogType));
        $view_log->log($iLogType, $idLog . " | " . $sInput . " | " . $sOutput);
    }

}
