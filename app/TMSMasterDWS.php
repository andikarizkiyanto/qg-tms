<?php

namespace App;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Librarys\LibStaticConnect;
use Carbon\Carbon;
use Cache;
use DB;

/**
 * Description of CoreUserRole
 *
 * @author user
 */
class TMSMasterDWS extends Model {

    //put your code here
    protected $table = 'tms_master_dws';
    protected $primaryKey = 'dws';

    public $incrementing=false;
    
    public static function getFlexiDWS($tanggal, $dws, $clock_in) {
        $oTanggal = Carbon::createFromFormat('Y-m-d', $tanggal);
        $oClockIn = Carbon::createFromFormat('H:i:s', $clock_in);
        $day = $oTanggal->__get("dayOfWeek");

        $oMasterDWSs = TMSMasterDWS::where('flexi_def_dws', 'like', "%$dws%")->where('flexi_def_day', 'like', "%$day%")->get();
        $selDWS = null;
        $iDiff = 60 * 60 * 4;
        $sRet = "";
        $lastResort = "";
        if (!empty($oMasterDWSs) && count($oMasterDWSs) > 0) {
            foreach ($oMasterDWSs as $oMasterDWS) {
                $oJamMasuk = Carbon::createFromFormat('H:i:s', $oMasterDWS->jam_masuk);
                $temp = $oClockIn->diffInSeconds($oJamMasuk, false);
                $dws = $oMasterDWS->attributes['dws'];
//                echo $dws." | ".$clock_in . " | " . $oMasterDWS->jam_masuk . " = " . $temp . " | ".$iDiff."<br/>";
                if ($temp < $iDiff && $temp >= 0) {
                    $iDiff = $temp;
                    $sRet = $oMasterDWS->attributes['dws'];
                }
                if ($oMasterDWS->jam_masuk == "09:00:00") {
                    $lastResort = $dws;
                }
            }
        }
        if (empty($sRet)) {
            return $lastResort;
        }
        return $sRet;
    }

    public static function getArrDWSFlexi() {
        return Cache::remember('DWS_FLEXI_LIST', 60 * 12, function() {
            $aRet = array();
            $oMasterDWSs = TMSMasterDWS::select('dws')->whereNotNull('flexi_def_dws')->get();
            foreach ($oMasterDWSs as $oDWS) {
                if(!empty($oDWS->dws)){
                    $aRet[] = $oDWS->dws;
                }
            }
            return $aRet;
        });
    }

    public static function getEligbleDWS($dws, $now) {
        $oMasterDWS = TMSMasterDWS::where('dws', $dws)->where('start_date', '<=', $now)->where('end_date', '>=', $now)->first();
        if (!empty($oMasterDWS)) {
            return $oMasterDWS;
        }
        return null;
    }

    public static function getDWS($dws) {
//        Cache::pull("DWS_LIST");
        $oDWS = Cache::remember('DWS_LIST', 60 * 12, function() {
                    return DB::table('tms_master_dws')->get();
                });
        if (empty($oDWS) || empty($dws)) {
            return "";
        } else {
            foreach ($oDWS as $row) {
                if ($row->dws == $dws) {
                    return $row;
                }
            }
            return "";
        }
    }

}
