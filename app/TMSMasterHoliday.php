<?php

namespace App;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Librarys\LibStaticConnect;
use Carbon\Carbon;
use Cache;

/**
 * Description of CoreUserRole
 *
 * @author user
 */
class TMSMasterHoliday extends Model {

    //put your code here
    protected $table = 'tms_master_holiday';
    protected $primaryKey = 'id';

    public static function isHoliday($tanggal) {
//        Cache::pull("MASTER_HOLIDAY");
        $oTMSMasterHoliday = Cache::remember('MASTER_HOLIDAY', 1440, function() {
                    return TMSMasterHoliday::get();
                });
        if (empty($oTMSMasterHoliday) || empty($tanggal)) {
            return false;
        } else {
            foreach ($oTMSMasterHoliday as $holiday) {
                if (trim($holiday->tgl) == trim($tanggal)) {
                    return true;
                }
            }
            return false;
        }
    }
    
    public static function getKeteranganHoliday($tanggal) {
        $oTMSMasterHoliday = Cache::remember('MASTER_HOLIDAY', 1440, function() {
                    return TMSMasterHoliday::get();
                });
        if (empty($oTMSMasterHoliday) || empty($tanggal)) {
            return false;
        } else {
            foreach ($oTMSMasterHoliday as $holiday) {
                if ($holiday->tgl == $tanggal) {
                    return $holiday->keterangan;
                }
            }
            return false;
        }
    }

}
