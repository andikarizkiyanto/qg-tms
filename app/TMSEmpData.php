<?php

namespace App;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Librarys\LibStaticConnect;
use App\Librarys\LibStaticContent;
use App\Librarys\LibStaticTMS;
use App\TMSAbsAtt;
use App\TMSMasterAbsAtt;
use App\TMSMasterDWS;
use DB;
use Carbon\Carbon;

/**
 * Description of CoreUserRole
 *
 * @author user
 */
class TMSEmpData extends Model {

    //put your code here
    protected $table = 'tms_emp_data';
    protected $primaryKey = 'data_id';

    public function getAbsAttDesc() {
        $oTMSAbsAtt = TMSAbsAtt::where('data_id', $this->data_id)->first();
        if (empty($oTMSAbsAtt)) {
            return "";
        }
        $oMasterAbsAtt = TMSMasterAbsAtt::where('subty', $oTMSAbsAtt->subty)->first();
        if (empty($oMasterAbsAtt)) {
            return "";
        }
        return $oMasterAbsAtt->text;
    }

    public function calc_came_late_back_early($oDWS) {
	$boolHoliday = TMSMasterHoliday::isHoliday($this->tanggal);
        if ($boolHoliday || $this->isAbsAtt || $this->dws == "OFF") {
            $this->came_late = 0;
            $this->back_early =0;
            return true;
        }
        if (!empty($this->clock_in)) {
            $oClockIn = Carbon::createFromFormat('H:i:s', $this->clock_in);
            $oJamMasuk = Carbon::createFromFormat('H:i:s', $oDWS->jam_masuk);
            $this->came_late = $oJamMasuk->diffInSeconds($oClockIn, false);
        }
        if (!empty($this->clock_out)) {
            $oClockOut = Carbon::createFromFormat('H:i:s', $this->clock_out);
            $oJamKeluar = Carbon::createFromFormat('H:i:s', $oDWS->jam_keluar);
            $this->back_early = $oClockOut->diffInSeconds($oJamKeluar, false);
        }
    }

    public function calc_work_second() {
        if (empty($this->clock_in) || empty($this->clock_out)) {
            return false;
        }
        $begtime = Carbon::createFromFormat("Y-m-d H:i:s", $this->tanggal . " " . $this->clock_in);
        $endtime = Carbon::createFromFormat("Y-m-d H:i:s", $this->tanggal . " " . $this->clock_out);

        $diff = $begtime->diffInSeconds($endtime, false);
        if ($diff < 0) {
            $endtime = $endtime->addDay();
            $diff = $begtime->diffInSeconds($endtime, false);
        }
        if ($diff > (4 * 60 * 60)) {
            $dws = $this->getDWSTable();
            $secondRest = 60 * 60;
            if (!empty($dws->rehat)) {
                $secondRest = Carbon::createFromFormat("H:i:s", $dws->rehat)->secondsSinceMidnight();
            }
            $this->work_second = $diff - $secondRest;
            $this->save();
        }
    }

    public static function spUpdateEmpData($nopeg, $time) {
        echo DB::raw("SELECT sp_update_data('" . $nopeg . "','" . $time . "')") . "<br/>";
        $ret = DB::select(DB::raw("SELECT sp_update_data('" . $nopeg . "','" . $time . "')"));
        if ($nopeg == "533670" && !empty($ret) && !empty($ret[0]) && $ret[0]->sp_update_data > 10) {
            $oTMSEmpData = TMSEmpData::where('data_id', $ret[0]->sp_update_data)->first();
            if (!empty($oTMSEmpData)) {
                $oTMSEmpData->calc_work_second();
            }
        }
        return $ret;
    }

    public function isTodayAndBefore() {
        $now = Carbon::now();
        $oDate = Carbon::createFromFormat("Y-m-d", $this->tanggal);
        $temp = $now->diffInDays($oDate, false);
        if ($temp <= 0) {
            return true;
        }
    }

    public function check_proses_flexytime() {
        
    }

    public function getHMSBackEarly() {
        $second = $this->getSecondBackEarly();
        if ($second <= 0)
            return "-";
        else
            return gmdate("H:i:s", $second);
    }

    public function getHMSComeLate() {
        $second = $this->getSecondComeLate();
        if ($second <= 0)
            return "-";
        else
            return gmdate("H:i:s", $second);
    }

    public function getHMSTotal() {
        $second = $this->getSecondTotal();
        if ($second <= 0)
            return "-";
        else
            return gmdate("H:i:s", $second);
    }

    public function getSecondComeLate() {
        $boolHoliday = TMSMasterHoliday::isHoliday($this->tanggal);
        if ($boolHoliday || $this->isAbsAtt || $this->dws == "OFF") {
            $this->came_late = 0;
            $this->save();
            return 0;
        }
        if (empty($this->came_late) && $this->isTodayAndBefore() && empty($this->clock_in)) {
            $this->came_late = 28800;
            $this->save();
        }
        if (empty($this->came_late) || $this->came_late <= 0)
            return 0;
        return $this->came_late;
//        return ($this->came_late > 28800) ? 28800 : $this->came_late;
    }

    public function getSecondBackEarly() {
        $boolHoliday = TMSMasterHoliday::isHoliday($this->tanggal);
        if ($boolHoliday || $this->isAbsAtt || $this->dws == "OFF") {
            $this->back_early = 0;
            $this->save();
        }
        if (empty($this->back_early) && $this->isTodayAndBefore() && empty($this->clock_out)) {
            $this->back_early = 28800;
            $this->save();
        }
        if (empty($this->back_early) || $this->back_early < 0)
            return 0;
        return ($this->back_early > 28800) ? 28800 : $this->back_early;
    }

    public function getSecondTotal() {
        $boolHoliday = TMSMasterHoliday::isHoliday($this->tanggal);
        if ($boolHoliday || $this->isAbsAtt || $this->dws == "OFF") {
            return 0;
        }
        $total = $this->getSecondComeLate() + $this->getSecondBackEarly();
        return ($total > 28800) ? 28800 : $total;
    }

    public function getSecondTotalDenda() {
        return $this->getSecondTotal();
    }

    public function getDWSTable() {
        $temp = TMSMasterDWS::getDWS(trim($this->dws));
        if (!empty($temp)) {
            return $temp;
        }
        return null;
    }

    public function getReasonText() {

        $sReason = "";
        if (!empty($thisisSubstitutions)) {
            $sReason.=" Substitusi";
        }
        if (!empty($this->isValidate)) {
            if ($sReason != "") {
                $sReason.=" | ";
            }
            $sReason.=" Validation";
        }
        if (!empty($this->isOvertime)) {
            if ($sReason != "") {
                $sReason.=" | ";
            }
            $sReason.=" Overtime";
        }
        if (!empty($this->isAbsAtt)) {
            if ($sReason != "") {
                $sReason.=" | ";
            }
            $sReason.=$this->getAbsAttDesc();
        }
        if(TMSMasterHoliday::isHoliday($this->tanggal)){
            $sReason.=TMSMasterHoliday::getKeteranganHoliday($this->tanggal);
        }
        return $sReason;
    }

    public function getPercentageSalaryCut() {
        $boolHoliday = TMSMasterHoliday::isHoliday($this->tanggal);
        if ($boolHoliday || $this->isAbsAtt || $this->dws == "OFF") {
            return 0;
        }
        $min = intval($this->getSecondTotal() / 60);
        if ($min < 0)
            return 0;
        return $min * (1 / (22 * 8 * 60));
    }

    public function insert_nopeg_dws($nopeg, $dws, $tanggal) {
        
    }

    public static function insert_array_nopeg_dws($nopeg, $aDWSs) {
        foreach ($aDWSs as $aDWS) {
            $oTMSEmpData = TMSEmpData::where('nopeg', $nopeg)->where('tanggal', $aDWS['tanggal'])->first();
            if (empty($oTMSEmpData)) {
//echo "STOPPP-1";exit;
                $oTMSEmpData = new TMSEmpData();
                $oTMSEmpData->nopeg = $nopeg;
                $oTMSEmpData->tanggal = $aDWS['tanggal'];
                $oTMSEmpData->dws = $aDWS['dws'];
                $oTMSEmpData->save();
            } else if (!empty($oTMSEmpData) && trim($oTMSEmpData->dws) != $aDWS['dws']) {
//echo "STOPPP-2";exit;
                $oTMSEmpData->dws = $aDWS['dws'];
                $oTMSEmpData->save();
            }
        }
    }

    public static function updateClockInClockOut($oAbsen) {
        $oRet = TMSEmpData::spUpdateEmpData($oAbsen->userid, substr(trim($oAbsen->tr_date), 0, 19));
        if (!empty($oRet) && !empty($oRet[0]) && !empty($oRet[0]->sp_update_data) && in_array($oAbsen->userid, LibStaticContent::getArrayNopegFlexi()) && $oRet[0]->sp_update_data > 99) {
            $oTMSEmpData = TMSEmpData::where('data_id', $oRet[0]->sp_update_data)->first();
            $checkDate = false;
            if (!empty($oTMSEmpData)) {
                $oFixDate = LibStaticContent::getStartFlexiDate();
                $oDate = Carbon::createFromFormat('Y-m-d', $oTMSEmpData->tanggal);
                $temp = $oFixDate->diffInDays($oDate, false);
                //LibStaticTMS::tmsLog($oFixDate . "#" . $oDate, "|" . $temp . "|");
                if ($temp >= 0) {
                    $checkDate = true;
                }
            }
            if ($checkDate && !empty($oTMSEmpData) && !empty($oTMSEmpData->clock_in)) {
                $sDws = TMSMasterDWS::getFlexiDWS($oTMSEmpData->tanggal, $oTMSEmpData->dws, $oTMSEmpData->clock_in);
                if (!empty($sDws) && $oTMSEmpData->dws != $sDws) {
                    $dws_ori = $oTMSEmpData->dws;
                    $oTMSEmpData->dws = $sDws;
                    $oTMSEmpData->isSubstitutions = 1;
                    $oTMSEmpData->modified_by = 'SyncSys';
                    $oTMSSubstitution = TMSSubstitution::insertSubstitution($oTMSEmpData->data_id, $dws_ori, $sDws);
                    TMSTask::insertTaskSubstitution($oTMSSubstitution, 0);
                    //$iTMS = LibStaticTMS::setDWS($oTMSEmpData->nopeg, $oTMSEmpData->data_id, $oTMSEmpData->dws);
                    $iTMS = LibStaticTMS::setDWSByTanggal($oTMSEmpData->nopeg, $oTMSEmpData->tanggal, $oTMSEmpData->dws);
                }
                $oTMSMasterDWS = TMSMasterDWS::where('dws', $oTMSEmpData->dws)->first();
                $oTMSEmpData->calc_came_late_back_early($oTMSMasterDWS);
                $oTMSEmpData->calc_work_second();
                $oTMSEmpData->save();
            }
        }
    }

    public static function reprosesEmpData($nopeg, $begda, $endda) {
        $oTemps = TMSEmpData::getEmpDataRange($nopeg, $begda, $endda);
        foreach ($oTemps as $oTemp) {
            $oTemp->clock_in = null;
            $oTemp->clock_out = null;
            $oTemp->save();
        }
        $oHKVerifieds = DB::connection('sqlsrv')->table('HKVERIFIED')->where('tr_date', '>=', $begda)->where('tr_date', '<=', $endda)->where('userid', $nopeg)->orderBy('tr_no', 'asc')->get();
        if (!empty($oHKVerifieds) && count($oHKVerifieds) > 0) {
            foreach ($oHKVerifieds as $oAbsen) {
                TMSEmpData::updateClockInClockOut($oAbsen);
            }
        }
    }

    public static function getEmpDataRange($nopeg, $begda, $endda) {
        $oTemps = TMSEmpData::where('nopeg', $nopeg)->where('tanggal', '>=', $begda)->where('tanggal', '<=', $endda)->get();
        if (empty($oTemps) || count($oTemps) == 0) {
            return null;
        }
        return $oTemps;
    }

    public static function getClockEnd($nopeg, $date) {
        $oTMSEmpData = TMSEmpData::where('nopeg', $nopeg)->where('tanggal', $date)->first();
        if (empty($oTMSEmpData))
            return "";
        $oTMSMasterDWS = TMSMasterDWS::where('dws', $oTMSEmpData->dws)->first();
        if (empty($oTMSMasterDWS))
            return "";
        return $oTMSMasterDWS->jam_keluar;
    }

    public function getTanggalUserFormat() {
        $tgl = Carbon::createFromFormat("Y-m-d", $this->tanggal);
        return $tgl->format('l, d.m.Y');
    }

    public function getTanggalFormatDMY() {
        $tgl = Carbon::createFromFormat("Y-m-d", $this->tanggal);
        return $tgl->format('d.m.Y');
    }

    public function getSecondWorkTime() {
        if ($this->isAbsAtt) {
            //perlu dicheck apakah absattnya
            $oAbsAtt = TMSAbsAtt::where('data_id', $this->data_id)->first();
            if (!empty($oAbsAtt)) {
                $oMasterAbsAtt = TMSMasterAbsAtt::getObject($oAbsAtt->subty);
                if (!empty($oMasterAbsAtt) && $oMasterAbsAtt->isAttendances) {
                    return 8 * 60 * 60;
                }
                if (!empty($oMasterAbsAtt) && $oMasterAbsAtt->isAbsences) {
                    return -1;
                }
            } else {
                return -1;
            }
        }
        $boolHoliday = TMSMasterHoliday::isHoliday($this->tanggal);
        if ($boolHoliday) {
            return -1;
        }

        if ((empty($this->clock_in) || empty($this->clock_out))) {
            return 0;
        }
        $oClockIn = Carbon::createFromFormat('H:i:s', $this->clock_in);
        $oClockOut = Carbon::createFromFormat('H:i:s', $this->clock_out);
        $res = TMSMasterDWS::getDWS($this->dws);
        $oJamMasuk = Carbon::createFromFormat('H:i:s', $res->jam_masuk);
        if ($oJamMasuk->diffInSeconds($oClockOut, false) < 0) {
            $oClockOut->addDay();
        }
        $secondWork = $oClockIn->diffInSeconds($oClockOut, false);

        if ($res->rehat == "01:00:00") {
            return $secondWork - (60 * 60);
        } else if ($res->rehat == "01:30:00") {
            return $secondWork - (90 * 60);
        }
        return $secondWork;
    }

    public static function getAverageTimeDataPeriod($sNopeg, $sBegda, $sEnd) {
        $oTMSEmps = TMSEmpData::where('nopeg', $sNopeg)->where('tanggal', '>=', $sBegda)->where('tanggal', '<=', $sEnd)->get();
        return TMSEmpData::getAverageTimeData($sNopeg, $oTMSEmps);
    }

    public static function getAverageTimeData($sNopeg, $oTMSEmps) {
        $aTemp = array("nopeg" => "", "nama" => "", "avg_time" => "", "tot_time" => "", "nday" => 0, "second" => 0, 'avg_second' => 0);
        $aTemp['nopeg'] = $sNopeg;
        $aTemp['nama'] = LibStaticContent::getEmpInfoNama($sNopeg);
        foreach ($oTMSEmps as $oTMSEmp) {
            $second = $oTMSEmp->work_second;
            if ($second >= 0) {
                $aTemp['nday'] ++;
                $aTemp['second']+=$second;
                if ($oTMSEmp->dws == "OFF") {
                    $aTemp['nday'] --;
                }
            }
        }
	if(!empty($aTemp) && !empty($aTemp['nday'])){
        $aTemp['avg_second'] = intval($aTemp['second'] / $aTemp['nday']);
	}else{
$aTemp['avg_second'] = 0;
		}
        $aTemp['avg_time'] = LibStaticContent::secondToTimeFormat($aTemp['avg_second']);
        return $aTemp;
    }

    public static function getTotalDenda($sNopeg, $oTMSEmps) {
        $aTemp = array("nopeg" => "", "nama" => "", "second_late" => 0, "tot_denda" => 0);
        $aTemp['nopeg'] = $sNopeg;
        $aTemp['nama'] = LibStaticContent::getEmpInfoNama($sNopeg);
        foreach ($oTMSEmps as $oTMSEmp) {
            $second = $oTMSEmp->getSecondTotalDenda();
            $denda = $oTMSEmp->getPercentageSalaryCut();
            $aTemp['second_late']+=$second;
            $aTemp['tot_denda']+=$denda;
//            echo $sNopeg." | ".$oTMSEmp->nopeg. " | ".$oTMSEmp->tanggal." | ".$second." | ".$denda;
//            echo " | ".$oTMSEmp->clock_in." | ".$oTMSEmp->clock_out." | ".$oTMSEmp->came_late." | ".$oTMSEmp->back_early;
//            echo "<br/>";
        }
        $aTempX = TMSEmpData::getAverageTimeData($sNopeg, $oTMSEmps);
        $aTemp['denda_time'] = LibStaticContent::secondToTimeFormat($aTemp['second_late']);
        $aTemp['avg_time'] = LibStaticContent::secondToTimeFormat($aTempX['avg_second']);
        return $aTemp;
    }

    public static function getTotalDendaPeriod($sNopeg, $sBegda, $sEnd) {
        $oTMSEmps = TMSEmpData::where('nopeg', $sNopeg)->where('tanggal', '>=', $sBegda)->where('tanggal', '<=', $sEnd)->get();
        return TMSEmpData::getTotalDenda($sNopeg, $oTMSEmps);
    }

    public static function getTotalCameLate($sNopeg, $oTMSEmps) {
        $aTemp = array("nopeg" => "", "nama" => "", "n_late" => 0);
        $aTemp['nopeg'] = $sNopeg;
        $aTemp['nama'] = LibStaticContent::getEmpInfoNama($sNopeg);
        foreach ($oTMSEmps as $oTMSEmp) {
            $second = $oTMSEmp->getSecondComeLate();
            if ($second > 0) {
                $aTemp['n_late'] ++;
            }
        }
        if ($aTemp['n_late'] > 0) {
            $aTempX = TMSEmpData::getAverageTimeData($sNopeg, $oTMSEmps);
            $aTemp['avg_time'] = LibStaticContent::secondToTimeFormat($aTempX['avg_second']);
        }
        return $aTemp;
    }

    public static function getTotalCameLatePeriod($sNopeg, $sBegda, $sEnd) {
        $oTMSEmps = TMSEmpData::where('nopeg', $sNopeg)->where('tanggal', '>=', $sBegda)->where('tanggal', '<=', $sEnd)->get();
        return TMSEmpData::getTotalCameLate($sNopeg, $oTMSEmps);
    }

    /*
      public function getPercentageSalaryCut(){
      $min = intval( $this->getSecondTotal()/60);
      if($min<0)return 0;
      return $min*(1/(22*8*60));

      } */
}
