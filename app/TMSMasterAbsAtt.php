<?php

namespace App;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Librarys\LibStaticConnect;
use App\TMSCutiTangguh;
use App\TMSAbsAtt;
use App\TMSMasterAbsAtt;
use Cache;

/**
 * Description of CoreUserRole
 *
 * @author user
 */
class TMSMasterAbsAtt extends Model {

    //put your code here
    protected $table = 'tms_master_abs_att';
    protected $primaryKey = 'subty';
    public $incrementing = false;

    public function isView($sNopeg, $tahun) {
        echo $this->subty . " | ";
        if ($this->subty == "0001") {
//            echo "HERE";
//            exit;
            $n = TMSCutiTangguh::getCountAvailCutiPengganti($sNopeg, $tahun);
            if ($n > 0) {
                $this->keterangan = "( Max " . $n . " Hari )";
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

//    public function getSubtyText($data_id){
//        $oAbsAtt = TMSAbsAtt::where('data_id',$data_id)->first();
//        if(empty($oAbsAtt)){
//            return "";
//        }
//        return TMSMasterAbsAtt::where()
//    }

    public static function getSubtyText($subty) {
        $oMaster = TMSMasterAbsAtt::getObject($subty);
        if (empty($oMaster))
            return "";
        return $oMaster->text;
    }

    public static function getObject($subty) {
        $oTMSMasterAbsAtt = Cache::remember('MASTER_ABS_ATT', 1440, function() {
                    return TMSMasterAbsAtt::get();
                });
        if (empty($oTMSMasterAbsAtt) || empty($subty)) {
            return null;
        } else {
            foreach ($oTMSMasterAbsAtt as $oMaster) {
                if ($oMaster->subty == $subty) {
                    return $oMaster;
                }
            }
            return null;
        }
    }
    
    public static function getIntegerAbsence($subty){
        $oAbbrv = TMSMasterAbsAtt::getObject($subty);
        if($oAbbrv->isAttendances){
            return 2;
        }
        return 1;
    }

//    public function isAuthView($a,$b){
//        echo " HERE";exit;
//    }
}
