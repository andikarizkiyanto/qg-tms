<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Session;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	
	 public function getSession() {
        return Session::get("SES" . $this->email);
    }
	
	  public function getSessionVariabel($sVar) {
        $a = $this->getSession();
        if(empty($a)){
            return "";
        }
        return $a[$sVar];
    }
}
