<?php

namespace App;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Librarys\LibStaticConnect;
use App\TMSUserDetail;
use App\TMSStructAddOn;
use App\TMSMasterOutsource;
use App\TMSEmpFast;
use Carbon\Carbon;
use App\Librarys\LibStaticContent;
/**
 * Description of CoreUserRole
 *
 * @author user
 */
class TMSNopegMaintain extends Model {

    //put your code here
    protected $table = 'tms_nopeg_maintain';
    protected $primaryKey = 'maint_id';

    var $aEmp=null;
    
    public static function delNopegMaintain($sNopeg) {
        TMSNopegMaintain::where('nopeg', $sNopeg)->delete();
    }

    public static function getPrimaryOrgMaintain($sNopeg, $isMan, $oEmp, $sDefConfigSub, $sMntType = "SORD") {
        $aOrg = "";
        // diambil dari table user_detail
        $aOrg = TMSUserDetail::getOrgMaintain($sNopeg, $sMntType);
        $oUser = Sentinel::getUser();
        // jika pejabat ambil dari org_unit ybs
        if ($isMan == 1) {
//            $aSupOrg->org_maint = $oEmp->org_unit;
            if ($sDefConfigSub == 3) {
                $aOrg[] = $oEmp->org_unit;
            }

            $aOrgAddOn = TMSStructAddOn::getUnitsArray($sNopeg);
            for ($i = 0; $i < count($aOrgAddOn); $i++) {
                $aOrg[] = $aOrgAddOn[$i];
            }
        }
        return $aOrg;
    }

    public static function getAllOrgMaintain($aOrg, $sDefConfigSub) {
        $sOrg = '-1';
        if ($aOrg) {
            foreach ($aOrg as $oOrg) {
                $sTmp = $oOrg;
                if ($sDefConfigSub == 3) {
                    $aTmp = LibStaticConnect::get_org_all($oOrg);
                    $sTmp = implode(",", $aTmp);
                } else if ($sDefConfigSub == 5) {
                    $aTmp = $this->get_org_down($oOrg, $sDefConfigSub);
                    $sTmp = implode(",", $aTmp);
                }
                $sOrg .= ($sOrg == "" ? "" : ",") . $sTmp;
            }
        }

        return $sOrg;
    }

    public static function genNopegMaintain($sNopeg, $sDegConfigSub, $oEmp) {
        TMSNopegMaintain::where('nopeg',$sNopeg)->delete();
        $aSub = LibStaticConnect::getSubordinate($sNopeg, $sDegConfigSub);
        $sSub = "";
        if ($aSub) {
            foreach ($aSub as $oSub) {
                $sSub .= ($sSub == "" ? "" : ",") . $oSub->nopeg;
            }
        }

        $oMan = LibStaticConnect::cekPejabat($sNopeg);
        $isLakhar = 0;
        $isMan = 0;
        if ($oMan) {
            $isMan = 1;
        }
        $oLakhars = TMSUserDetail::getLakhar($sNopeg);
        if ($isLakhar)
            $isMan = 1;
        $sOrg="";
        $aOrg = TMSNopegMaintain::getPrimaryOrgMaintain($sNopeg, $isMan, $oEmp, $sDegConfigSub, "SORD");
        if (!empty($aOrg)) {
            for ($i = 0; $i < count($aOrg); $i++) {
                if (empty($sOrg)) {
                    $sOrg = $aOrg[$i];
                } else {
                    $sOrg .= ($aOrg[$i] == "" ? "" : ",") . $aOrg[$i];
                }
                $aSub = LibStaticConnect::getSubordinateFromOrg($aOrg[$i], $sDegConfigSub);
                if ($aSub) {
                    foreach ($aSub as $oSub) {
                        $sSub .= ($sSub == "" ? "" : ",") . $oSub->nopeg;
                    }
                }
            }
        }
        if ($isMan == 1) {
            if (empty($sOrg))
                $sOrg = $oEmp->org_unit;
            else
                $sOrg .="," . $oEmp->org_unit;
        }

        TMSNopegMaintain::update_nopeg_maintain($sNopeg, $sOrg, $sSub, "SORD");

        $user = Sentinel::getUser();
        if ($user->isTimeAdmin == "1") {
            $sOrgTAdm = "";
            $sOrgTAdm = TMSNopegMaintain::getAllOrgMaintain($aOrg, $sDegConfigSub);
            TMSNopegMaintain::update_nopeg_maintain($sNopeg, $sOrgTAdm, $sSub, "TADM");
        }
        
        if($isLakhar==1 || $isMan==1 || $user->isTimeAdmin=="1"){
            LibStaticContent::genEmpInfo($user->email);
        }

        return array("isMan" => $isMan, "isLakhar" => $isLakhar);

//
//
//        $oMan = $this->CI->global_m->cekPejabat($sNopeg);
//        $isLakhar = 0;
//        $isMan = 0;
//        if ($oMan) {
//            $isMan = 1;
//        }
//        //else{
//        $isLakhar = $this->CI->global_m->cekLakhar($sNopeg);
//        if ($isLakhar)
//            $isMan = 1;
//        $oEmp = $this->CI->global_m->getEmpInfo($sNopeg);
//
//        $sOrg = "";
//        // find org maintain
//        $aOrg = TMSNopegMaintain::getPrimaryOrgMaintain($sNopeg, "SORD");
//        if (!empty($aOrg)) {
//            for ($i = 0; $i < count($aOrg); $i++) {
//                if (empty($sOrg)) {
//                    $sOrg = $aOrg[$i]->org_maint;
//                } else {
//                    $sOrg .= ($aOrg[$i]->org_maint == "" ? "" : ",") . $aOrg[$i]->org_maint;
//                }
//                $aSub = $this->CI->global_m->getSubordinateFromOrg($sNopeg, $aOrg[$i]->org_maint);
//                if ($aSub) {
//                    foreach ($aSub as $oSub) {
//                        $sSub .= ($sSub == "" ? "" : ",") . $oSub->nopeg;
//                    }
//                }
//            }
//        }
////if($sNopeg=="521538"){
////var_dump($oEmp->org_unit	);exit;
////	}
//        if ($isMan == 1) {
//            if (empty($sOrg))
//                $sOrg = $oEmp->org_unit;
//            else
//                $sOrg .="," . $oEmp->org_unit;
//        }
////}else{
////		$sOrg = $this->CI->global_m->getAllOrgMaintain($sNopeg,"SORD");
////}
//        // update nopeg_maintain di tms_user_detail
//
//        $this->CI->global_m->update_nopeg_maintain($sNopeg, $sOrg, $sSub, "SORD");
//        
//        $user = Sentinel::getUser();
//        if ($oEmp->isTimeAdmin == "1") {
//            $sOrgTAdm = "";
//            $sOrgTAdm = $this->CI->global_m->getAllOrgMaintain($sNopeg, "TADM");
//            $this->CI->global_m->update_nopeg_maintain($sNopeg, $sOrgTAdm, $sSub, "TADM");
//        }
//        $oTemp = LibStaticConnect::getSubordinate($sNopeg, $sDegConfigSub);
//        TMSNopegMaintain::where('nopeg', $email)->delete();
    }

    public static function update_nopeg_maintain($sNopeg, $sOrg = '', $sSub = '', $sMntType = 'SORD') {
        // inputan : organisasi dari tms_nopeg_maintain plus klo dia atasan masukin nomer pegawai bawahannya
        TMSNopegMaintain::where('nopeg', $sNopeg)->where('mnt_type', $sMntType)->delete();
        $now = Carbon::now()->toDateString();
        if ($sOrg <> '') {
//            $sSelfOrg = "-1";
//            $sTmp = ($sSub <> "" ? " UNION SELECT '" . $sNopeg . "', nopeg, NOW(), 1,'" . $sMntType . "' FROM tms_master_emp WHERE nopeg IN(" . $sSub . ") AND EE_subgrp NOT IN('6A','6J','5A','5H','5L','5R','5S','5T') AND EE_grp <> 'X' AND nopeg<>'" . $sNopeg . "' " : "");
//            // karyawan garuda
//            $sQuery = "INSERT INTO tms_nopeg_maintain(nopeg,nopeg_maintain,last_change,isEmp,mnt_type) " .
//                    "SELECT '" . $sNopeg . "', nopeg, NOW(), 1,'" . $sMntType . "' FROM tms_master_emp " .
//                    "WHERE org_unit IN(" . $sOrg . ") 
//			AND position_key NOT IN(select SOBID from tms_new.tms_master_relation where OBJID IN($sSelfOrg) AND OTYPE='O' AND SUBTY='B012' AND CURDATE() BETWEEN BEGDA AND ENDDA) 
//			AND nopeg<>$sNopeg AND EE_subgrp NOT IN('6A','6J','5A','5H','5L','5R','5S','5T') AND EE_grp <> 'X'  " . $sTmp . "; ";
//            $this->db->query($sQuery);

            $aSub = explode(",", $sSub);
            $aOrg = explode(",", $sOrg);
            if (count($aSub) > 0) {
                for ($i = 0; $i < count($aSub); $i++) {
                    $oTemp = TMSNopegMaintain::where('nopeg',$sNopeg)->where('nopeg_maintain',$aSub[$i])->where('mnt_type',$sMntType)->first();
                    if(empty($oTemp)){
                        $oNopegMaintain = new TMSNopegMaintain();
                        $oNopegMaintain->nopeg = $sNopeg;
                        $oNopegMaintain->nopeg_maintain = $aSub[$i];
                        $oNopegMaintain->isEmp = 1;
                        $oNopegMaintain->mnt_type = $sMntType;
                        $oNopegMaintain->save();
                    }
                }
            }


            // karyawan outsourcing
//            $sQuery = "INSERT INTO tms_nopeg_maintain(nopeg,nopeg_maintain,last_change,isEmp,mnt_type) " .
//                    "SELECT '" . $sNopeg . "', nopeg, NOW(), 0,'" . $sMntType . "' FROM tms_master_outsource " .
//                    "WHERE org_unit IN(" . $sOrg . ") AND nopeg <> '" . $sNopeg . "' ; ";
//            $res = $this->db->query($sQuery);
            if (count($aOrg) > 0) {
                $oEmps = TMSMasterOutsource::select('nopeg')->whereIn('org_unit', $aOrg)
                                ->where('start_date', '<=', $now)->where('end_date', '>=', $now)->get();
                if (!empty($oEmps) && count($oEmps) > 0) {
                    foreach ($oEmps as $oEmp) {
                        $oTemp = TMSNopegMaintain::where('nopeg',$sNopeg)->where('nopeg_maintain',$oEmp->nopeg)->where('mnt_type',$sMntType)->first();
                        if(empty($oTemp)){
                            $oNopegMaintain = new TMSNopegMaintain();
                            $oNopegMaintain->nopeg = $sNopeg;
                            $oNopegMaintain->nopeg_maintain = $oEmp->nopeg;
                            $oNopegMaintain->isEmp = 0;
                            $oNopegMaintain->mnt_type = $sMntType;
                            $oNopegMaintain->save();
                        }
                    }
                }
            }

            // karyawan fast moving
//            $sQuery = "INSERT INTO tms_nopeg_maintain(nopeg,nopeg_maintain,last_change,isEmp,mnt_type) " .
//                    "SELECT '" . $sNopeg . "', nopeg, NOW(), isEmp,'" . $sMntType . "' FROM tms_emp_fast " .
//                    "WHERE org_unit IN(" . $sOrg . ") AND nopeg <> '" . $sNopeg . "' AND CURDATE() BETWEEN start_date AND end_date " .
//                    "AND nopeg NOT IN(SELECT nopeg_maintain FROM tms_nopeg_maintain WHERE nopeg = '" . $sNopeg . "');";
//            $this->db->query($sQuery);
            if (count($aOrg) > 0) {
                $oEmps = TMSEmpFast::select('nopeg', 'isEmp')->whereIn('org_unit', $aOrg)
                                ->where('start_date', '<=', $now)->where('end_date', '>=', $now)->get();
                if (!empty($oEmps) && count($oEmps) > 0) {
                    foreach ($oEmps as $oEmp) {
                        $oTemp = TMSNopegMaintain::where('nopeg',$sNopeg)->where('nopeg_maintain',$oEmp->nopeg)->where('mnt_type',$sMntType)->first();
                        if(empty($oTemp)){
                            $oNopegMaintain = new TMSNopegMaintain();
                            $oNopegMaintain->nopeg = $sNopeg;
                            $oNopegMaintain->nopeg_maintain = $oEmp->nopeg;
                            $oNopegMaintain->isEmp = $oEmp->isEmp;
                            $oNopegMaintain->mnt_type = $sMntType;
                            $oNopegMaintain->save();
                        }
                    }
                }
            }

            // delete from tms_nopeg maintain jika ada karyawan sudah pindah di fast moving
//            $sQuery = "DELETE FROM tms_nopeg_maintain " .
//                    "WHERE nopeg = '" . $sNopeg . "' " .
//                    " AND nopeg_maintain IN(SELECT nopeg FROM tms_emp_fast " .
//                    "WHERE org_unit NOT IN(" . $sOrg . ") AND CURDATE() BETWEEN start_date AND end_date); ";
//            $this->db->query($sQuery);
//            }
        } elseif ($sSub <> "") { // kayana ga bakalan masuk kesini deh :)
            // hanya sebagai atasan saja, tidak maintain org_unit manapun
            // karyawan garuda
            $aSub = explode(",", $sSub);
            $aOrg = explode(",", $sOrg);
            if (count($aSub) > 0) {
                for ($i = 0; $i < count($aSub); $i++) {
                    $oNopegMaintain = new TMSNopegMaintain();
                    $oNopegMaintain->nopeg = $sNopeg;
                    $oNopegMaintain->nopeg_maintain = $aSub[$i];
                    $oNopegMaintain->isEmp = 1;
                    $oNopegMaintain->mnt_type = $sMntType;
                    $oNopegMaintain->save();
                }
            }
//            $sQuery = "INSERT INTO tms_nopeg_maintain(nopeg,nopeg_maintain,last_change,isEmp,mnt_type) " .
//                    "SELECT '" . $sNopeg . "', nopeg, NOW(), 1," . $sMntType . " FROM tms_master_emp WHERE nopeg IN(" . $sSub . "); ";
//            $this->db->query($sQuery);
        }

//        if ($this->db->trans_status() === FALSE) {
//            $this->db->trans_rollback();
//
//            return FALSE;
//        } else {
//            $this->db->trans_commit();
//            return TRUE;
//        }
    }
    
    public function getEmpMaintainNama(){
        return $this->getEmpMaintainInfo("nama");
    }
    public function getEmpMaintainPositionName(){
        return $this->getEmpMaintainInfo("position_name");
    }
    public function getEmpMaintainShort(){
        return $this->getEmpMaintainInfo("short");
    }
    public function getEmpMaintainStext(){
        return $this->getEmpMaintainInfo("stext");
    }
    public function getAEmpMaintainInfo(){
        if(empty($this->aEmp)){
            $this->aEmp = LibStaticContent::getEmpInfo($this->nopeg_maintain);
        }
        
        if(empty($this->aEmp)){
            return null;
        }
        return $this->aEmp;
    }
    
    public function getEmpMaintainInfo($field){
        if(empty($this->aEmp)){
            $this->aEmp = LibStaticContent::getEmpInfo($this->nopeg_maintain);
        }
        
        if(empty($this->aEmp)){
            return "Nama";
        }
        return $this->aEmp->{$field};
    }



    public function getNama(){
        return $this->getDataNama("nama");
    }
    public function getDataNama(){
        if(empty($this->aEmp)){
            $this->aEmp = LibStaticContent::getEmpNama($this->nopeg_maintain);
        }
        
        if(empty($this->aEmp)){
            return "Nama";
        }
        return $this->aEmp;    
    }

}
