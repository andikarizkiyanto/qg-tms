<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use DB;
use Sentinel;
use Carbon\Carbon;
use Response;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Librarys\LibStaticContent;
use App\TMSEmpData;
use App\Librarys\LibStaticConnect;
use App\TMSMasterDWS;
use App\TMSUserDetail;
use App\TMSNopegMaintain;

use Auth;

class TAdmController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function getTimedata(Request $request) {
         $nav = 'Time Admin';
        $title = 'Time Admin';
        $month = date("n");
        if (!empty($request->input('month'))) {
            $month = $request->input('month');
        }
        $year = date("Y");
        if (!empty($request->input('year'))) {
            $year = $request->input('year');
        }
        $nopeg_maintain = "";
        if (!empty($request->input('nopeg'))) {
            $nopeg_maintain = $request->input('nopeg');
        }

        $user =  Auth::user()->email;
        $pos = LibStaticContent::getDataSessionEmp($user);
        $selectedEmp = LibStaticContent::getDefaultEmpInfo();
		$orgMaintain = TMSUserDetail::where('nopeg', $user)->where('isActive', '1')->get();
		$org = $orgMaintain[0]->org_maint;
		$empMaintain = LibStaticConnect::getSubordinateFromOrg($org);
		
			if (!empty($empMaintain) && count($empMaintain) > 0) {
            $flag_found = false;
            if (empty($nopeg_maintain)) {
                $nopeg_maintain = $empMaintain[0]->nopeg;
               # $selectedEmp = $empMaintain[0]->getAEmpMaintainInfo();
                $flag_found = true;
            } else {
                foreach ($empMaintain as $emp) {
                    if ($emp->nopeg == $nopeg_maintain) {
                 #       $selectedEmp = $emp->getAEmpMaintainInfo();
                        $flag_found = true;
                        break;
                    }
                }
            }
            if ($flag_found == false) {
                $nopeg_maintain = "";
            }
        }
        $timedata = null;


        if (!empty($empMaintain) && !empty($nopeg_maintain)) {
            $timedata = TMSEmpData::where('nopeg', $nopeg_maintain)
                            ->whereMonth('tanggal', '=', $month)->whereYear('tanggal', '=', $year)
                            ->orderBy('tanggal', 'desc')->get();
            $aKey = array();
            $aReason = array();
            if (!empty($timedata) && count($timedata) > 0) {
                foreach ($timedata as $data) {
                    $aKey[$data->dws] = $data->getDWSTable();
                    $aReason[$data->data_id] = $data->getReasonText();
                }
            }
        }


        $month_bef = $month - 1;
        $year_bef = $year;
        if ($month_bef == 0) {
            $month_bef = 12;
            $year_bef--;
        }
        $month_next = $month + 1;
        $year_next = $year;
        if ($month_next == 13) {
            $month_next = 1;
            $year_next++;
        }
        $sMonth = LibStaticContent::getMonthDesc($month);
//            var_dump($selectedEmp);exit;
        $param = array("nopeg_maintain" => $nopeg_maintain, "empMaintain" => $empMaintain, "selectedEmp" => $selectedEmp,
            "month" => $month, "year" => $year, "month_bef" => $month_bef, "year_bef" => $year_bef, "month_next" => $month_next,
            "year_next" => $year_next, "smonth" => $sMonth);
            
            #$data['nama'] = LibStaticContent::getDataSessionEmp($nopeg_maintain);
           # $nama =  $data['nama'];
    
       return View('admin/time_data', compact('aData', 'param', 'timedata', 'title','nav',"aReason", "aKey", "pos","nopeg_maintain","nama"));
   /*
      return ([
	//	'param' => $orgMaintain[0]->org_maint,
		'param' => $nopeg_maintain,
		'message' => 'Hello world!'
      ]);
  */
        }
  

}
