<?php
namespace App\Http\Controllers;
use App\ModelUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
class SigninController extends Controller
{
    //
    public function index(){
        if(!Session::get('login')){
            return redirect('login')->with('alert','Kamu harus login dulu');
        }
        else{
            return view('index');
        }
    }
    public function login(){
        return view('login');
    }
    public function loginPost(Request $request){
        $aEEGrpBlock = array("X", "T", "P");
        $aEESubGrpBlock = array("2T", "2U", "2Z", "4A", "4H", "5A", "5H", "5L", "5R", "5S", "5U", "5V", "5W", "6A", "6J");
        $aNoSPKL = array('2B', '6B', '5B', '99', 'F', 'G', 'FSM', 'FA', 'FO', 'CP');


        $email = $request->email;
        $password = $request->password;
        $data = ModelUser::where('email',$email)->first();
        if(count($data) > 0){ //apakah email tersebut ada atau tidak
            if(Hash::check($password,$data->password)){
		#	if(check($password,$data->password)){
                $aSession = array();
                $user = $data->email;

                if (empty($user) || $user->isStatus != 1) {
                    return Redirect::route('login')->with('error', 'not authorized');
                }

                if ($user->isOutsource == 1) {
                    $aSession = LibStaticContent::getDataSessionOutsource($user);
                    $aSession['can_espkl'] = 0;
                } else {
                    $oEmp = LibStaticConnect::getEmpData($request->input($user));
                    if (empty($oEmp) || in_array($oEmp->{"EE_grp"}, $aEEGrpBlock) || in_array($oEmp->{"EE_subgrp"}, $aEESubGrpBlock)) {
                        return Redirect::route('login')->with('error', 'not authorized');
                    }
                    $aSession = LibStaticContent::getDataSessionEmp($user, $oEmp);
//                    echo $oEmp->{"EE_grp"};exit;
                    if ($oEmp->{"EE_grp"} == "P" || $oEmp->{"job_key"} <= 40000201 || $oEmp->{"PS_group"} == 'A' || $oEmp->{"PS_group"} == 'F' || $oEmp->{"PS_group"} == 'G' || (in_array($oEmp->{"EE_subgrp"}, $aNoSPKL) && in_array($oEmp->{"PS_group"}, $aNoSPKL)))
                        $aSession['can_espkl'] = 0;
                    else
                        $aSession['can_espkl'] = 1;
                    if ($oEmp->{"PS_group"} == 'A' || $oEmp->{"EE_subgrp"} == '5A')
                        $aSession['can_leave'] = 0;
                    else
                        $aSession['can_leave'] = 1;
                }
                if (empty($aSession)) {
                    return Redirect::route('login')->with('error', 'Server has been difficulty to load your data');
                }

                

                $aSession['s_man'] = $aRetGenNopegMaintain['isMan'];
                $aSession['s_lakhar'] = $aRetGenNopegMaintain['isLakhar'];
                $aSession['username'] = $data->email;
                $aSession['nama'] = $oEmp->{"nama"};
                $aSession['userid'] = $data->email;
                $aSession['isTAdmin'] = $data->isTimeAdmin;
                $aSession['isSAdmin'] = $data->isSuperAdmin;
                $aSession['isWeb'] = $data->isWeb;
                $aSession['isOutsource'] = $data->isOutsource;
//                $aSession['isVP'] = $user->isOutsource;

              #  $data->generateSession($aSession);       
           #     Session::put('email',$data->email);

            #    Session::put('login',TRUE);
                
                              
                return redirect('home');
            
            
            
            }
            else{
                return redirect('login')->with('alert','Password atau Email, Salah !'.Hash::check($password,$data->password));
            }
        }
        else{
            return redirect('login')->with('alert','Password atau Email, Salahaa!');
        }
    }
    public function logout(){
        Session::flush();
        return redirect('login')->with('alert','Kamu sudah logout');
    }
    public function register(Request $request){
        return view('register');
    }
    public function registerPost(Request $request){
        $this->validate($request, [
            'email' => 'required|min:4|email|unique:users',
            'password' => 'required',
            'confirmation' => 'required|same:password',
        ]);
        $data =  new ModelUser();
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->save();
        return redirect('login')->with('alert-success','Kamu berhasil Register');
    }
}