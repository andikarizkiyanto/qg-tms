<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Librarys\LibStaticConnect;
use App\Librarys\LibStaticContent;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user =  Auth::user()->email;
        $pos = LibStaticContent::getDataSessionEmp($user);
        $title = 'Dashboard';
        $nav = 'Home';

        return view('index', compact('title','nav','pos'));
		

    }
}
