<?php

namespace App\Http\Controllers;

//use App\Models\PAEmpData;
//use App\Models\PAEmpOrg;
//use App\Models\PAEmpOvt;
//use App\Models\PAEmpSal;
use Illuminate\Support\Facades\Redirect;
use DB;
use Sentinel;
use Carbon\Carbon;
use Response;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Librarys\LibStaticContent;
use App\TMSEmpData;
use App\Librarys\LibStaticConnect;
use App\TMSMasterDWS;
use App\TMSValidate;
use Auth;


class EmpController extends Controller {


    public function __construct()
    {
        $this->middleware('auth');
    }


    public function downloadTimedata(Request $request) {
     
			$user =  Auth::user()->email;
            $month = date("n");
            if (!empty($request->input('month'))) {
                $month = $request->input('month');
            }
            $year = date("Y");
            if (!empty($request->input('year'))) {
                $year = $request->input('year');
            }
            $nowCurr = Carbon::createFromDate($year, $month, 01);
            $sFilename = $user . " | " . $nowCurr->format("F Y");
            $data = array();
            $data['now'] = $nowCurr;
            $data['nopeg'] = $user;
            Excel::create($sFilename, function($excel) use ($data) {
                $excel->sheet($data['now']->format('F Y'), function($sheet) use ($data) {
                    $sheet = LibStaticContent::genSheetSingleNopegSingleMonth($sheet, $data['nopeg'], $data['now']);
                });
            })->download('xlsx');
        
    }

    public function getTimedata(Request $request) {
            
            $nav = 'Employee';
            $title = 'Time Data';
            $month = date("n");
            if (!empty($request->input('month'))) {
                $month = $request->input('month');
            }
            $year = date("Y");
            if (!empty($request->input('year'))) {
                $year = $request->input('year');
            }
			
			$user =  Auth::user()->email;
            $pos = LibStaticContent::getDataSessionEmp($user);
		
            
            $timedata = TMSEmpData::where('nopeg', $user)
                            ->whereMonth('tanggal', '=', $month)->whereYear('tanggal', '=', $year)
                            ->orderBy('tanggal', 'desc')->get();
            $aKey = array();
            $aReason = array();
            if (!empty($timedata) && count($timedata) > 0) {
                foreach ($timedata as $data) {
                    $aKey[$data->dws] = $data->getDWSTable();
                    $aReason[$data->data_id] = $data->getReasonText();
                }
            }
            $month_bef = $month - 1;
            $year_bef = $year;
            if ($month_bef == 0) {
                $month_bef = 12;
                $year_bef--;
            }
            $month_next = $month + 1;
            $year_next = $year;
            if ($month_next == 13) {
                $month_next = 1;
                $year_next++;
            }
            $sMonth = LibStaticContent::getMonthDesc($month);
            $param = array("month" => $month, "year" => $year, "month_bef" => $month_bef, "year_bef" => $year_bef, "month_next" => $month_next, "year_next" => $year_next, "smonth" => $sMonth);
            return View('employee/time_data', compact('aData', 'param', 'timedata','nav', 'title', "aReason", "aKey", "pos"));
   /*
      return ([
		'param' => Auth::user(),
		'message' => 'Hello world!'
      ]);
  */


    }

       public function getManualAttandance(Request $request) {
			$year = date("Y");
            $statusError1 = false;
            $statusError2 = 0;
            $sMessageError1 = "";
            $sMessageError2 = "";
            $aPost = $request->all();
            if ($request->isMethod('get') && !empty($aPost['year'])) {
                $sMessageError1 = LibStaticContent::genValidation($aPost, [
                            'year' => 'required|int',
                ]);
                if (empty($sMessageError1)) {
                    if (!empty($request->input('year'))) {
                        $year = $request->input('year');
                    }
                } else {
                    $statusError1 = true;
                    return Redirect::to($request->fullUrl())->with("st_error_1", $statusError1)->with("st_error_2", $statusError2)->with("serror_1", $sMessageError1)->with("serror_2", $sMessageError2);
                }
            }

			$user =  Auth::user()->email;
            $aSup['nopeg'] = $sNopegSup = LibStaticContent::getNopegSuperior($user);
            $aSup['nama'] = LibStaticContent::getEmpNama($sNopegSup);
            //CHECK POST
            if ($request->isMethod('post')) {
                $sType = $request->input('type');
                if ($sType == "manattprop") {
                    return $this->proposeManualAttendance($request, $user, $sNopegSup, $aPost, $statusError1, $sMessageError1);
                } else if ($sType == "manattcanc") {
                    return $this->cancelManualAttandance($request, $user, $aPost, $statusError1, $sMessageError1);
                }
                $statusError1 = true;
                $sMessageError1 = "Unknown Parameter Form";
                return Redirect::to($request->fullUrl())->with("st_error_1", $statusError1)->with("st_error_2", $statusError2)->with("serror_1", $sMessageError1)->with("serror_2", $sMessageError2);
            }
            $aStatusMessage = $this->getStatusMessage($request);


            #$validates = TMSValidate::whereHas('data', function($query) use ( $year, $user) {
            #            $query->where('nopeg', $user)->whereYear('tanggal', '=', $year);
            #        })->orderBy('data_id', 'desc')->get();
            $aKey = array();
            $aReason = array();
            $year_bef = $year - 1;
            $year_next = $year + 1;
            $now = Carbon::now();
            $sNow = $now->format('d.m.Y');
            $sNowSql = $now->format('Y-m-d');
            $oEmpData = TMSEmpData::getEmpDataRange($user, $sNowSql, $sNowSql);
            $oMasterDWS = TMSMasterDWS::getEligbleDWS($oEmpData[0]->dws, $sNowSql);
            $full_url = $request->fullUrl();
            $param = array(
                "full_url" => $full_url, "now" => $sNow, "year" => $year, "year_bef" => $year_bef, "year_next" => $year_next,
                "validates" => $validates, 'master_dws' => $oMasterDWS);
            $param = array_merge($param, $aStatusMessage);
            return View('employee.manual_att', compact('param', "aReason", "aKey", "aSup"));
	
	   }
	   
	   
	    private function getStatusMessage(Request $request) {
        $statusError1 = false;
        $statusError2 = 0;
        $sMessageError1 = "";
        $sMessageError2 = "";
        $temp_e1 = Session::get('st_error_1');
        $temp_e2 = Session::get('st_error_2');
        $temp_s1 = Session::get('serror_1');
        $temp_s2 = Session::get('serror_2');
//echo $temp_e1." | ".$temp_e2." | ".$temp_s1." | ".$temp_s2;exit;
        if ($temp_e1) {
            $statusError1 = $temp_e1;
        }
        if ($temp_e2) {
            $statusError2 = $temp_e2;
        }
        if ($temp_s1) {
            $sMessageError1 = $temp_s1;
        }
        if ($temp_s2) {
            $sMessageError2 = $temp_s2;
        }
        return array("st_error_1" => $statusError1, "st_error_2" => $statusError2, "serror_1" => $sMessageError1, "serror_2" => $sMessageError2);
    }


}
