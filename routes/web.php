<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('index');
Route::get('/leave', 'LeaveController@getLeave');
Route::get('/leave', 'LeaveController@getLeave');


Route::get('time_data', 'EmpController@getTimedata');
Route::get('dl_time_data', 'EmpController@downloadTimedata');

Route::get('manual_att', 'EmpController@getManualAttandance');



Route::get('time_admin', 'TAdmController@getTimedata');

Route::get('/suptime_data', 'SupController@getTimedata');

//Route::group(['prefix' => 'sup'], function () {
//    Route::get('/', array('as' => 'sup', 'uses' =>'SupController@getTimedata'));
//    Route::get('time_data', 'SupController@getTimedata');
//    });


Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );
